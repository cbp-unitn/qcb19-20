---
layout: home
---

Welcome to the *hands-on* section for the course of **Computational Biophysics**
at the University of Trento for the a.y. 2019-2020.

<p class="prompt prompt-info"> Check the newest course [here](/) </p>


This website will collect all the tutorials presented during hands-on sessions.
The material presented here will guide you in all practical steps needed to setup 
and run a simulation of a protein, and interpret its results.

Tutorials will be uploaded each Thursday evening before the lecture. 

# List of tutorials and sources

1. **[2019.09.16 - Introduction & Visualisation with VMD](/QCB/tutorial1)**
2. **[2019.09.22 - Fundamental ingredients of an MD simulation](/QCB/tutorial2)**
3. **[2019.10.07 - Setting up a project + scripting your analyses](/QCB/tutorial3)**
4. **[2019.10.14 - Setting a Namd simulation with thermostate and solvent](/QCB/tutorial4)**
5. **[2019.10.21 - Namd@HPC, setting up Recoverin simulations](/QCB/tutorial5)**
6. **[2019.10.28 - Recoverine wildtype, Recoverine mutations and git](/QCB/tutorial6)**
7. **[2019.11.11 - Preliminary results + Membrane proteins 1.0](/QCB/tutorial7)**
8. **[2019.11.18 - Membrane proteins 2.0](/QCB/tutorial8)**
9. **[2019.11.25 - Membrane proteins 3.0 + mutated Recoverine in the membrane](/QCB/tutorial9)**
10. **[2019.12.02 - Graphs, proteins, pyinteraph web servers for computational biophysics](/QCB/tutorial10)**
11. **[2019.12.09 - Molecular Dynamics Flexible Fitting](/QCB/tutorial11)**
12. **[2019.12.16 - Steered Molecular Dynamics](/QCB/tutorial12)**

# Conventions 
Throughout the tutorial we will use the following colour code[^1].

[^1]: The idea is taken from the colour scheme used by the [Bonvin Lab](http://www.bonvinlab.org/education/molmod/) for their course in Molecular Dynamics.

<p class="prompt prompt-attention"> Pay attention to this!</p>
<p class="prompt prompt-info"> Useful(?) information. </p>
<p class="prompt prompt-link"> Useful links. </p>
<p class="prompt prompt-question"> Think about this question. </p>
<p class="prompt prompt-shell"> Bash shell commands/outputs</p>
<p class="prompt prompt-tk"> VMD tcl/tk console.</p>

To improve readability, scripts will be formatted as:
```bash
#!/bin/bash

echo "This is a bash script."
```

# Further readings
- [A paper on how to organise a computational biology project](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1000424)
- [Bash and ViM cheatsheet](/cs)


