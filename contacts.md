---
title: Contacts
layout: page
---

### Mod. 1: Physical modeling of Biomolecules (theoretical)
**Prof. Gianluca Lattanzi**  
gianluca.lattanzi _at_ unitn.it  

### Mod. 2: Computer simulations of Biomolecules (practical)
**Prof. Luca Tubiana**  
luca.tubiana _at_unitn.it  

### Teaching assistant
**Dott. Marco Giulini**  
marco.giulini _at_ unitn.it  
