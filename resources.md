---
title: Resources
layout: page
---

In this page you can find additional resources to help you increase your knowledge of some particular topics.

## Bash 
* [Bash beginners' guide](https://www.tldp.org/LDP/Bash-Beginners-Guide/html/sect_01_05.html). 
* [Manual](https://www.gnu.org/software/bash/manual/). 
* [Bash cheatsheet](https://devhints.io/bash). A useful page collecting most bash commands.

## TCL
* [TCL for beginners](https://wiki.tcl-lang.org/page/TCL+for+beginners)

## VMD
* [VMD user guide](https://www.ks.uiuc.edu/Research/vmd/current/ug/)

## NAMD
* [download NAMD](/materials/NAMD_Git-2019-08-29_Linux-x86_64-multicore.tar.gz)
* [Download Charmm ff](https://mackerell.umaryland.edu/charmm_ff.shtml)
* [NAMD user guide](https://www.ks.uiuc.edu/Research/namd/2.12/ug.pdf)

## Awk
* [A very, very short intro to awk](/QCB/extras/awk)
