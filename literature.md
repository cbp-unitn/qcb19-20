---
title: Literature
layout: page
---

Here you can find some suggested readings on the topics covered during the course.

# Fundamental concepts in protein physics
* [**Folding Graciously**](materials/literature/proteins_levinthal_1969.pdf). Levinthal C, Proc. Mössebaun Spect. Biol. Sys. (1969).
* [**Studies on Protein Folding, Unfolding and Fluctuations by Computer Simulation**](materials/literature/taketomi2009.pdf). Taketomi H, Ueda Y, and Go N. Int J Pep. Prot. Res. (1974).
* [**Funnels, Pathways, and the Energy Landscape of Protein Folding: A Synthesis**](materials/literature/bryngelson1995.pdf). Bryngelson JD et al. Proteins (1995).

# Sampling & enhanced sampling methods
* [**Essential Dynamics of Proteins**](materials/literature/amadei1993.pdf). Amadei A, Linssen ABM, Berendsen HJC. Proteins (1993).
* [**Equilibrium Sampling in Biomolecular Simulations**](materials/literature/zuckerman2011.pdf). Zuckerman DM. Annu. Rev. Biophys. (2011).
* [**Simplified and improved string method for computing the minimum energy paths in barrier-crossing events**](materials/literature/string_JCP.pdf). Weinian E, Eiquing R, Vanden-Ejinden E. Jour Chem Phys (2007).
* [**Ratcheted molecular-dynamics simulations \[...\] protein folding**](materials/literature/tiana_camilloni.pdf). Tiana G and Camilloni C. Jour. Chem. Phys. (2012).

# Computational analysis methods
* [**Quantifying uncertainty and sampling quality in biomolecular simulations**](materials/literature/nihms155855.pdf). Grossfield A and Zuckerman DM. Annu Rep Comp Chem (2009).
* [**A tutorial on Principal Component Analysis**](https://arxiv.org/pdf/1404.1100.pdf). Shlens J. ArXiv (2014).
* [**Principal Component Analysis: A Method for Determining the Essential Dynamics of Proteins**](materials/literature/nihms739932.pdf). David CC and Jacobs DJ. Methods Mol Biol (2014).
* [**PyInteraph: A Framework for the Analysis of Interaction Networks in Structural Ensembles of Proteins**](materials/literature/ci400639r.pdf). Tiberti M et al. Chem. Info and Model. (2014).

# Example applications 
* [**Molecular mechanisms of activation in CDK2**](materials/literature/Molecular mechanisms of activation in CDK2.pdf). Bešker N, Amadei A, D'Abramo M. Jour. Biomol. Struc. Dyn. (2013). 
* [**Alanine mutation of \[...\] Pantothenate Synthetase \[...\]**](materials/literature/Alanine_mutation_of_the_catalytic_sites_of_Pantoth.pdf). Pandey B et al. Nat. Sci. Rep. (2017).
* [**Effects of Membrane and Biological Target on the \[...\] Properties of Recoverin\[...\]**](materials/literature/ijms-20-05009.pdf). Borsatto A, et al, IJMS (2019).

