## Contributing material to the repository
To contribute new material or correct existing problems, please follow the steps below.

1. Create a new branch starting from master
2. Make your changes **only** in the new branch
3. After cheking compatibility with master, push a Merge Request (MR) on gitlab, assigning it to 
Luca Tubiana for review. 
4. Reply to all comments from the reviewer, until the MR is accepted/discarded.

### Basig branch manament with git
A git branch is basically an independent snapshot of the repository. All changes performed in this snapshot will
not affect other ones. Using branches has several benefits, among which:

* sandbox modification to source.
* one branch can be protected from unauthorized changes ("source of truth").
* high parallelization of work.
* merging makes conflicts evident.

The **basic commands** to use branches are the following.

```bash
git branch [-a]               	  # print existing branches (-a includes remote branches)
git checkout <branch-name>    	  # checkout an existing branch, including remote ones. Rung git-fetch first.
git checkout -b <branch-name> 	  # create a new branch <branch-name>
git checkout -D <branch-name> 	  # delete the branch <branch-name>
git diff <branch-A> -- <branch-B> # print the diff betweeb <branch-A> and <branch-B>
git merge <branch-name>           # merge FROM <branch-name> INTO the local branch.
```

### An example of the git workflow used for this repo.
The following steps exemplifies how to add a new lesson to the repository, for example lesson 2.

#### 1. Create the new branch.
```bash
git checkout master && git pull   # update master
git checkout -b lecture_4         # create new branch locally 
git push --set-upstream origin lecture_4  # set new branch on server
... do your work git add , git commit, ....
``` 

Please note that the material prepared by Gianfranco is already present in master, it is just not visible on
the web-page as there is no link pointing to it. A version of the page with all links on is present in branch
`all_material`.

#### 2. Once you are happy with your changes, prepare the MR.
```bash
git checkout master && git pull  # update master in case it changed.
git checkout lecture_4
git merge master                 # merge FROM master INTO lecture_4
# solve all conflicts!
git push
```

#### 3. Using gitlab branch page, propose a Merge Request.
1. Open `Repository`->`branches` from the left menu. 
2. Click on the `merge request` button on the line corresponding to your branch.
3. Write a short report describing the reason for your MR. It should be a few lines, describing all changes and the reasons for them. 
4. Assign the MR to Luca Tubiana as a reviewer. 
5. Reply to all comments by the reviewer.

### Some useful references on git branches and workflows.

1. [git branches: official documentation](https://git-scm.com/book/it/v2/Git-Branching-Basic-Branching-and-Merging)
2. [git workflow](https://nvie.com/posts/a-successful-git-branching-model/) this is a standard workflow for git.





