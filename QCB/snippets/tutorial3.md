## How a CBP project should be organized or, 'the three laws of CBP'.
Before proceeding further, let's discuss a bit how to set up a project so
that it is **reproducible**, **efficient**, and **scalable**. Think of these 
as if they where the three laws of ~~robotics~~ CBP.  

1. The results of a research project must be **reproducible**.   
2. The protocol leading to the results should be as **efficient**, as long as this
  efficiency does not hinder reproducibility.   
3. The results of a research project should produce an impact at **scale**, as
   long as this does not conflict with the reproducibility and efficiency. 

In practice, for a computational project based on simulations, these points
mean the following.

1. The project is structured in such a way that it is easy to recover data and to
  understand how every piece of information is obtained. 
2. Obtaining the data or re-running the analyses on new sets of data should
   take as little time as possible to the researcher. 
3. The topic allows for further research by other people. The protocol
   makes it easy to add new analyses and integrate results in a larger,
   cooperational project.

A consequence of following all three is that it should become clear, with
experience, when an idea you are exploring will result in a good research given
the limited resources you have (first of all, your own time).

## Script & Automate everything you can.
Computers and automation exist so that we can spend less time doing repetitive
tasks.  Those vary from vacuuming the floor to solving difficult equations to
simulating entire systems (_efficiency_).

Another advantage of automating tasks is that -if done properly- you know
exactly how they are executed (_reproducibility_). 

If you do not try to reinvent the wheel, build consistently on top of
existing software, and frequently discuss techniques and aims with peers your 
research and protocol will spread more easily (_scalability_).
