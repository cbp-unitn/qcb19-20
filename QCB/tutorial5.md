---
layout: page
author: Gianfranco Abrusci
mathjax: true
---
<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>

In this session we will build the setup for an explicit solvent simulation,
 analyse the configuration file, launch it on a cluster, and perform a small analysis of a system.[^1]

[^1]: See [tutorial 3](/pages/QCB/tutorial3) for the references.


## NAMD@HPC & setting up the recoverin simulation.
In this lecture:
1. Finalizing the explicit solvent simulation setup from last time.
2. How to use the cluster.
   - Architecture.
   - Usage.
   - Send and retrieve data from the cluster.
   - Setting up a NAMD environment (project template, reloaded).
3. Starting on recoverin.
   - Building a slightly more complex psf.
   - Simulation in implicit solvent (on the cluster).
   - Solvate and start simulation in explicit solvent (on the cluster).

### Explicit solvent (part 2)
Last time we built the setup for an explicit solvent simulation.

<p class="prompt prompt-question">What steps did we follow?</p>

### Let's finish our last tutorial
Let's run the simulation. As a guideline, let's remind the procedure
we should follow for a production simulation:
1. minimisation;
2. NVT simulation to go into the canonical ensemble;
3. Add the pressure.

<p class="prompt prompt-attention">NAMD does not recognize the charmm parameter file for water and ions</p>

You should use the _toppar_water_ions_namd.str_ file instead. You can find it inside your vmd directory at _PATH_TO_VMD_DIRECTORY/Contents/vmd/plugins/noarch/tcl/readcharmmpar1.4/_ .It will also be present in the directory that we are going to distribute to you.

We will run an _NpT_ simulation just to get accustomed to the new option.
You already know what lines you should fill, but now we have two more things to add.

<p class="prompt prompt-attention">Use the centered box!!!</p>

First, now our system has also water molecules, therefore we need a new
parameter file.
It's in the _VMD_ plugin folder.

Second, we have to set the center and the `a b c` of the box.
The center should be `0.0 0.0 0.0` because of one of the previous commands,
and the _cellVectors_ are

```tcl
cellBasisVector1 _Delta x_  0.0  0.0
cellBasisVector2   0.0   _Delta y_ 0.0
cellBasisVector3   0.0  0.0  _Delta z
```

## Thermostat for NVT simulations in NAMD

To perform an NVT simulation, we need to use a thermostat.
For _NAMD_, the algorithm we will employ is the Langevin thermostat:

$$m \frac{d^2 x}{dt^2} = -\nabla U(x) - \gamma \frac{dx}{dt} + \sigma \xi$$

where $$\gamma$$ is the damping coefficient, $$\xi$$ is a random number with 0
mean and standard deviation 1. The parameter $$\sigma$$ is related to the
damping coefficient by the fluctuation-dissipation theorem:

$$\sigma^2 = 2 k_B T m \gamma$$.

What we will set is the **damping coefficient** $$\gamma$$ (1/ps). The higher
the value of this coefficient, the smaller are the fluctuations of the
temperature. But then the system is highly perturbed.
As a rule of thumb, the value of $$\gamma$$ should be set to the smallest
In general, a value between 1 and 10 ps$$^{-1}$$ should be fine.
value that preserves the wanted temperature (on average).

We have also an option to couple the thermostat to the hydrogen atoms
of the system. Since we constrain the same atoms, we will disable it.


## Launching the simulation

Launch `namd` using 2 or more cores:
<p class="prompt prompt-shell">$ namd2 +pN conf.namd > lognameN.log & </p>
where `N` is the number of cores available. Try it with 4 cores and leave it
running The simulation should take ~50min for 100k steps. So, launch 1000 time
steps of simulation to check if it works.


A longer trajectory (10 ns) for bpti in explicit solvent 
is available [here](https://drive.google.com/file/d/1vP-DcCvr3YVEv0XCXkAtfWwhCAEOOFrn/view?usp=sharing).


## Using HPC@UniTN

This is a very small guide about launching NAMD simulations on `HPC@Unitn`.
It is meant to be *helpful* for the master students who attended _Computational
Biophysics_.

You are supposed to have a basic knowledge of bash/sh (i.e. using the terminal).
You can find resources on bash under the resources section.

For more info about the cluster,
see [HPC website](https://sites.google.com/unitn.it/hpc/home).

# How to connect
In order to connect to the HPC cluster, you have to be connected to the
university network:
- you are connected to Unitn-x or similar (so you are in one of the
  university structures);
- if you log from home, you need to use the university VPN (install the
  Pulse secure and google how to connect to unitn).

Once one of the aforementioned criteria is fulfilled, then open a shell
(Ctrl+Alt+t for Linux user) and type:

``` bash
 ssh <your-unitn-username>@hpc2.unitn.it
```
and then your password will be requested.

**NOW** you are connected to the login _computers_.
**DO NOT LAUNCH JOBS THERE**: those computers are meant just for the job
submission, since they are shared among all connected users and they have to
be available.

If you want to do something on the fly, using only few cores for few minutes,
go interactively (see below).


#  A minimal intro to cluster architecture
{:refdef: style="text-align: center;"}
![a standard cluster architecture](../../img/tut5/standard_HPC.jpg)  
_A standard cluster architecture._
{:refdef}
A cluster usually have:
* Some login nodes to connect to the rest of the world.
* A file server + disks to store data.
* Computational nodes, to perform heavy calculations.
* Hopefully, an infiniband connection to make the nodes communicate with each other.
* A batch scheduler (PBS for our HPC cluster) to manage the computational jobs of different users fairly.

{:refdef: style="text-align: center;"}
![Cores, cpus, and nodes](../../img/tut5/cores_and_nodes.jpg)  
_Cores, CPUs, nodes. (image adapted from [Supalov et al.](https://link.springer.com/chapter/10.1007/978-1-4302-6497-2_2))_
{:refdef}
Inside a node you will have one or more CPUs, each with one or more nodes. The cpus usually share
some very fast access memory while the ram is shared per node. This will affect some of you in the 
future if you end up writing and optimizing your own code (hint: you might want to avoid that).

# An extremely quick overview of PBS
The cluster has a batch scheduler, a software that organises the execution of
the jobs coming from different users with different requested resources.
The allocation is based on **resources** those you need, and those that are free.

**Resources** are:  
* Nodes types (some have more cores, some have GPUs for example).
* \# Nodes.
* \# Cores.
* Amount of memory.
* Walltime: maximum time you will be granted; if your job is not ended yet, it will be killed. So, choose it carefully and use the `Benchmark` NAMD provides you for a good estimate.

These resources are usually rationalized in **queues**. That is, the cluster administration can set up
some limits on the walltime for some specific resources. You can specify a queue (better) or let the 
scheduler pick up a default queue for you, based on which resources you ask. To see  the queues, you can run:
```bash
qstat -S
```

We remind you that **you should request maximum 16 cores.**
As far as I know, you will not be able to run, statistically, those jobs that request more
than 10 cores per nodes.
Therefore, either you ask for 10 cores only on a single node (the following
  line will be clear in a while):
```bash
#PBS -l select=1:ncpus=10:mpiprocs=10:mem=40GB
```
or you ask for 10 cores on two nodes (using then 20 cores in total)
```bash
#PBS -l select=2:ncpus=10:mpiprocs=10:mem=40GB
```
We will see later where you can find the Template batch files for PBS.


**IMPORTANT**: before launching _THE_ simulation, just run small simulations (1000 timesteps maximum)
asking for different numbers of cores. For example: launch 3 simulations
requesting 5, 10 e 20 cores (_ça va sans dire_, set the walltime at 10 mins).
Depending on your system, you will have a small gain in performances using 20 cores with
respect to 10 cores. If this is your case, use only 10 cores, since it will speed
up your time in queue.

### Submitting a batch job
To launch a calculation on the nodes, you need to instruct the batch scheduler about
your intentions: what resources you need, and what you need to run. 

```bash
#!/bin/bash
#PBS -l select=1:ncpus=10:mpiprocs=10:mem=10GB  # Resources you need
#PBS -l walltime=00:10:00            # WALLTIME!
#PBS -q short_gpuQ                   # Queue
#PBS -N USE_AN_APPROPRIATE_NAME      # Name of the job
#PBS -o appropriate_name_out         # stdout redirected here
#PBS -e appropriate_name_err         # stderr redirected here

# This is a comment.
# From the /home in the compute node we move to the
# directory from which we launched the job
# (and where you, hopefully, have your files)

cd $PBS_O_WORKDIR

namd2 +p10 +setcpuaffinity +devices 0 conf.namd > log.log
```

Suppose this file is called `submit_me.pbs`. You can submit the job with:
```
qsub submit_me.pbs
```
Then you usually wait, have a cup of coffee and pray.

####  What to modify
All the lines starting with `#PBS` are **NOT** comments but instructions for
the batch scheduler.

```
#PBS -l select=<numberOfNodes>:ncpus=<numberOfCoresPerNode>\
:mpiprocs=<sameNumberOfNcpus>:mem=<GBofRAMnecessary>

#PBS -l walltime=hours:minutes:seconds

#PBS -q <queue to be used>

#PBS -N <name to be visualised using qstat>

#PBS -o/e <file to redirect stdout and stderr>
```

### Interactive jobs
On the login shell type:
```bash
qsub -I -q <queue> -l select=1:ncpus=2:mpiprocs=2:mem=10GB,walltime=00:30:00
```
and then you can use the same commands you use in the `submit_me.pbs` file.


### Check your job(s) and delete them if necessary
The command  to check for resources, jobs, statistics, etc. is `qstat`. 
To see what it does:
```bash
qstat -h
```

To check all the jobs running for your user.
```bash
qstat -u
```

Once you submitted a job, you can delete it. 
```bash
qdel <jobid>
```

# Copying to/from the cluster

See the cluster [page]().

You can use: 
* `scp`: `cp` over ssh protocol.  
  `scp <file(s)> <user>@<destination_address>:<destination_dir>`  
   e.g. `scp myfile.txt luca.tubiana@hpc2.unitn.it:` 
* `rsync`: rsync is useful to sync folders.  
  `rsync -avuz <files(s)> <user>@<destination_address>:<destination_dir>`
* `wget` to download from webservers.
* `git` is available only through the `http` protocol.

`scp` works as `cp`: if you want to upload or download a folder you have to use
the option `-r`.

Please note the `:` at the end of the command: the colon represents your `home`.
If you want to save a file in another directory you should use
<p class="prompt prompt-shell">$ scp file your_username@hpc.unitn.it:path/to/a/folder</p>

As usual, use the man page or the command's help to get more info on the commands!
* `man <command>`,
* `<command> -h`.

# Using modules
A capital problem for shared systems: different users need different softwares, diffierent libraries,
different compilers. The solution for this is to isolate the "software stacks" you use from those of the system.
I.e. make you able to use a certain version of the libraries/software you need, on demand. There are two main
solutions for this.

1. Using **[modules](http://modules.sourceforge.net/)**.
2. Using **containers** like [docker](https://www.docker.com/) and [singularity](https://sylabs.io/docs/). 

Modules are supported everywhere right now. The downside is that what is
available is decided by the admin. Containers can instead be set by the user,
but they are not yet a standard in academia.  Docker is the standard in
industry, but poses serious security problems on shared clusters. Singularity
tries to alleviate them. We will give you a quick intro to modules.

1. See **available modules:** `module avail`.
2. **Load** a module to use it: `module load <module_name>`. 
3. **List** loaded modules: `module list`.
4. **Unload** a module: `module unload <module_name>`
5. **Purge** all loaded modules: `module purge`.

## Setting up the environment on the cluster.
Remember the folder structure introduced in lecture 3? Well, we kept the idea,
and completely changed the structure. You can now install it where you want, plus
it will semi-automatically download NAMD, VMD, miniconda, the recoverin files, and
the PBS template scripts to use the cluster. For those interested, we used 
[cookiecutter](https://cookiecutter.readthedocs.io/en/latest/) to set  up the project template.

Let's install everything..

#### I. Install cookiecutter
```bash
module load python-<version> ... #use the above commands to find a version of python >= 3.6!
pip3 install --user cookiecutter
```

#### II. Use cookiecutter to get the folder
```bash
cd <some_directory_I_like>
cookiecutter git+http://jupyterlab.physics.unitn.it/gitlab/luca.tubiana/cbp-recoverin-project.git
```
Cookiecutter will ask you a few questions, including your name, your group, and
your email. Fill them in correctly.

You will end up with a folder like this.

```bash
└── G01-recoverin-mutation
    ├── AUTHORS.md
    ├── bin
    │   ├── activate.sh                # activate conda env.
    │   ├── get_conda.sh               # download and install miniconda
    │   ├── get_gdrive_resources.py    # get NAMD, Charmm parameters, VMD
    │   ├── git_initialize.sh          # DO not use for now!!
    │   └── prepare.sh                 # Prepare the software stack
    ├── conda_QCB_essentials.yml       # Used to create conda's image
    ├── G01-data
    │   ├── 00-external                # External data
    │   │   └── recoverin
    │   │       ├── README.md
    │   │       ├── rec_grk1.pdb
    │   │       └── rec_r.pdb
    │   ├── 01-raw                     # Simulations go here
    │   │   ├── 01-implicit
    │   │   ├── 02-mutated-implicit
    │   │   ├── 03-mutated-explicit
    │   │   └── 04-mutated-membrane
    │   ├── 02-processed               # Post-processed stuff here (repeat folder structure above)
    │   └── 03-analyzed                # Analyzed results (repeat folder structure)
    ├── G01-doc
    ├── G01-notebooks
    ├── G01-references
    │   └── Readme.md
    ├── G01-report
    │   ├── imgs
    │   └── report_template.tex
    ├── G01-src                        # Source code + external software and useful templates
    │   ├── analysis
    │   ├── external                   # NAMD, VMD, toppar, will be installed here
    │   ├── tools                      # Any scripts that helps you
    │   │   └── Templates              # Templates files for HPC & NAMD
    │   │       ├── HPC_short_cpu.sh
    │   │       ├── HPC_short_gpu.sh
    │   │       └── namd_sample.conf
    │   └── visualization              # Visualization scripta
    ├── makefile                       # Makefile: run a dependency pipeline.
    └── README.md                      # The mutation you need to study is written here.
```

To finalize the installation, run `make all` from the base directory. This will
download the needed software stack and guide you through the installation of
miniconda.


## Further info

For more commands and the use of the batch scheduler:
- go to [HPC@Unitn guide](https://sites.google.com/unitn.it/hpc/architettura);
- type `man qsub` or `qsub -h` in the shell.

##  A gentle and friendly reminder
You will be able -and you are encouraged- to use GPU nodes for your simulations,
especially for those related to your final project. But do not forget that the 
cluster is a shared machine used for actual research by the whole university.  

Use it responsibly. 

Just a final friendly reminder on this subject. 


<p class="prompt prompt-attention">We will monitor your usage of the cluster. <br>
<br>
Any inappropriate usage will be reported<br> to the admins and the professors. <br>
<br> We don't forgive. <br> We don't forget. </p>




## Recoverin: let's have fun with the pdb

We gave you a pdb with two calcium ions and a myrystoylated glycine residue. Try to load it in VMD and to authomatically build the topology file.

<p class="prompt prompt-question">Do you obtain anything meaningful?</p>

We should be particularly careful while building the psf in this case.

1. Inside the PDB the myrystoylated glycine is set as GLM, but CHARMM parameter files (_toppar_all36_lipid_prot.str_) call it GLYM.
2. We have to build a topology object for each of the chains

Let's go through this process step-by-step.

<p class="prompt prompt-attention">Remember to log the commands in order to obtain a tcl script</p>

I. load the molecule

<p class="prompt prompt-tk">% mol new rec_r.pdb</p>

II. separate the calcium ions from the protein

```tcl
set calcium [atomselect top "name CAL"]
$calcium writepdb calcium.pdb
```
III. rename the residue and write a pdb with only-protein atoms.

```tcl
set glym [atomselect top "resname GLM"]
$glym set resname GLYM
set protein [atomselect top "protein or resname GLYM"]
$protein writepdb protein.pdb
```

IV. load the topology files

```tcl
package require psfgen
resetpsf
topology _your_path_to_VMD_top/top_all36_prot.rtf
topology _your_path_to_VMD_top/top_all36_lipid.rtf
topology _your_path_to_VMD_top/top_all36_na.rtf
topology _your_path_to_VMD_top/top_all36_carb.rtf
topology _your_path_to_VMD_top/top_all36_cgenff.rtf
topology _your_path_to_toppar/stream/lipid/toppar_all36_lipid_prot.str
topology ./toppar_water_ions_namd.str
```

V. build the protein segment

```tcl
segment P {pdb protein.pdb 
	first none 
	last CNEU}
coordpdb protein.pdb P
```

VI. build the calcium-ions segment

```tcl
segment I {auto none 
	pdb calcium.pdb}
coordpdb calcium.pdb I
```

VII. guess coordinates and build psf and pdb

```tcl
guesscoord
writepsf my_rec_r.psf
writepdb my_rec_r.pdb
```

## Next steps:

The idea for the rest of this lecture is to redo the simulations for bpti on
recoverin. On the cluster. So:
1. Run a simulation of recoverin in implicit solvent.
2. Solvate recoverin and add the ions. For recoverin, the ionic concentration we are intrested in is 150 mM KCl and 1 mM MgCl2 (one Mg ion is enough).
3. Launch an NVT simulation of the solvated recoverin. Use different number of processors and measure efficiency.

Now you should have all the tools to perform a benchmark of your system.
Use 2/5/10 cores to simulate 1000 steps of your system.

<p class="prompt prompt-question">Is there any difference?</p>
<p class="prompt prompt-question">Compare the benchmark for the 2 cores with
and without gpus.</p>

## For next time (do it!)
Each group should:
1. Launch a simulation in implicit solvent of recoverin (use 4 cores).
2. Launch a simulation in explicit solvent (use 16 cores).
3. Launch a batch of simulation (explicit solvent) to check the effectiveness of NAMD with
   different number of cores and with GPUs:
   - 4/8/16 cores
   - 8 GPU cores.

Use the folders correctly! Next time we will be able to compare the trajectories among you.

<p class="prompt prompt-question">Compute the radius of gyration of
the protein in implicit and explicit solvent.</p>


## Notes
