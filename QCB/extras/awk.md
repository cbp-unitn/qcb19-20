---
layout: page
authors: [Gianfranco Abrusci, Tubiana Luca]
---

## A quick intro to AWK 
_AWK_ is a scripting language to manipulate text. It is particular useful for small
tasks that do not require a lot of lines of code.

Compared to Python _awk_ has a major pro, and a major con:
* **pro** _awk_ is extremely fast when working on CSV and other text files.
* **con** _awk_ code is not nearly as easy to maintain as Python code.

The basic syntax of an _awk_ instruction is:
```bash
awk ' condition {some actions}'  input.file1 input.file2 ... input.fileN
```
_awk_ will loop over the lines of the file(s) and perform the actions you wrote,
if _condition_ is satisfied. No condition implies `True`.

The default variables you will need are:
- `NF`: number of fields (i.e. columns separated as default by blank spaces);
- `NR`: number of rows (i.e. lines), the counter over which the default
 loop is performed;
- `$0`: a whole line;
- `$i`: the i-th field;
Moreover you can do operations before (`BEGIN`) and after (`END`)
 the main loop.

The Field and Row separators can be redefined either in `BEGIN` or by passing them
as an option, see `man awk`. Furthermore, you can pass arguments from `bash` to `awk`
using the option `-v var=$var` where `$var` is some variable set in `bash`.

Awk further support standard flow control structures as in `C`.

# Example 
First, create a file with 1 column filled with numbers from 1 to 100.
**Hint**: use a bash for-loop.

Then, let's compute the sum and the average.
```bash
awk 'BEGIN{sum=0} {sum += $1} END{print "sum:", sum, "\navg:", sum/NR}'  gauss_spicciame_casa.dat
```
See _Notes_ for more information and fancy things[^2].

[^2]: Rtfm on `man awk`, or use Google/StackOverflow.
