# MDANALYSIS
- python package for molecular dynamics data analysis
- easy and custom
- computationally expensive tasks are written in C


```python
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from os import fspath
%matplotlib inline
```


```python
import MDAnalysis as mda
mda.__version__
```




    '0.20.1'



### optional: latex packages
- if you want to show a notebook to your supervisor
- if you want to write down a couple of equations quickly


```python
#from IPython.display import Latex
#from tabulate import tabulate
#from IPython.display import HTML
```


```python
Path
```




    pathlib.Path




```python
!tar xvf tmp_tutorial3.tar.gz
```

    adk_equilibrium/
    adk_equilibrium/adk_dims.dcd
    adk_equilibrium/adk_open.pdb
    adk_equilibrium/1ake_007-nowater-core-dt240ps.dcd
    adk_equilibrium/adk_closed.pdb
    adk_equilibrium/adk4AKE.psf


## CREATION OF A UNIVERSE
This is the main object. It stores MD data as numpy arrays, so that we can perform any operation using fast numpy libraries


```python
simdir = Path('./adk_equilibrium/')
PSF = simdir / 'adk4AKE.psf'
eqDCD = simdir / 'adk_dims.dcd'
PDB = simdir / 'adk_open.pdb'
```


```python
u = mda.Universe(str(PSF), str(eqDCD))
```


```python
help(u)
```

    Help on Universe in module MDAnalysis.core.universe object:
    
    class Universe(builtins.object)
     |  The MDAnalysis Universe contains all the information describing the system.
     |  
     |  The system always requires a *topology* file --- in the simplest case just
     |  a list of atoms. This can be a CHARMM/NAMD PSF file or a simple coordinate
     |  file with atom informations such as XYZ, PDB, Gromacs GRO, or CHARMM
     |  CRD. See :ref:`Supported topology formats` for what kind of topologies can
     |  be read.
     |  
     |  A trajectory provides coordinates; the coordinates have to be ordered in
     |  the same way as the list of atoms in the topology. A trajectory can be a
     |  single frame such as a PDB, CRD, or GRO file, or it can be a MD trajectory
     |  (in CHARMM/NAMD/LAMMPS DCD, Gromacs XTC/TRR, or generic XYZ format).  See
     |  :ref:`Supported coordinate formats` for what can be read as a
     |  "trajectory".
     |  
     |  As a special case, when the topology is a file that contains atom
     |  information *and* coordinates (such as XYZ, PDB, GRO or CRD, see
     |  :ref:`Supported coordinate formats`) then the coordinates are immediately
     |  loaded from the "topology" file unless a trajectory is supplied.
     |  
     |  Examples for setting up a universe::
     |  
     |     u = Universe(topology, trajectory)          # read system from file(s)
     |     u = Universe(pdbfile)                       # read atoms and coordinates from PDB or GRO
     |     u = Universe(topology, [traj1, traj2, ...]) # read from a list of trajectories
     |     u = Universe(topology, traj1, traj2, ...)   # read from multiple trajectories
     |  
     |  Load new data into a universe (replaces old trajectory and does *not* append)::
     |  
     |     u.load_new(trajectory)                      # read from a new trajectory file
     |  
     |  Select atoms, with syntax similar to CHARMM (see
     |  :class:`~Universe.select_atoms` for details)::
     |  
     |     u.select_atoms(...)
     |  
     |  Parameters
     |  ----------
     |  topology : str, Topology object or stream
     |      A CHARMM/XPLOR PSF topology file, PDB file or Gromacs GRO file; used to
     |      define the list of atoms. If the file includes bond information,
     |      partial charges, atom masses, ... then these data will be available to
     |      MDAnalysis. A "structure" file (PSF, PDB or GRO, in the sense of a
     |      topology) is always required. Alternatively, an existing
     |      :class:`MDAnalysis.core.topology.Topology` instance may also be given.
     |  topology_format
     |      Provide the file format of the topology file; ``None`` guesses it from
     |      the file extension [``None``] Can also pass a subclass of
     |      :class:`MDAnalysis.topology.base.TopologyReaderBase` to define a custom
     |      reader to be used on the topology file.
     |  format
     |      Provide the file format of the coordinate or trajectory file; ``None``
     |      guesses it from the file extension. Note that this keyword has no
     |      effect if a list of file names is supplied because the "chained" reader
     |      has to guess the file format for each individual list member.
     |      [``None``] Can also pass a subclass of
     |      :class:`MDAnalysis.coordinates.base.ProtoReader` to define a custom
     |      reader to be used on the trajectory file.
     |  all_coordinates : bool
     |      If set to ``True`` specifies that if more than one filename is passed
     |      they are all to be used, if possible, as coordinate files (employing a
     |      :class:`MDAnalysis.coordinates.chain.ChainReader`). [``False``] The
     |      default behavior is to take the first file as a topology and the
     |      remaining as coordinates. The first argument will always always be used
     |      to infer a topology regardless of *all_coordinates*. This parameter is
     |      ignored if only one argument is passed.
     |  guess_bonds : bool, optional
     |      Once Universe has been loaded, attempt to guess the connectivity
     |      between atoms.  This will populate the .bonds .angles and .dihedrals
     |      attributes of the Universe.
     |  vdwradii : dict, optional
     |      For use with *guess_bonds*. Supply a dict giving a vdwradii for each
     |      atom type which are used in guessing bonds.
     |  is_anchor : bool, optional
     |      When unpickling instances of
     |      :class:`MDAnalysis.core.groups.AtomGroup` existing Universes are
     |      searched for one where to anchor those atoms. Set to ``False`` to
     |      prevent this Universe from being considered. [``True``]
     |  anchor_name : str, optional
     |      Setting to other than ``None`` will cause
     |      :class:`MDAnalysis.core.groups.AtomGroup` instances pickled from the
     |      Universe to only unpickle if a compatible Universe with matching
     |      *anchor_name* is found. Even if *anchor_name* is set *is_anchor* will
     |      still be honored when unpickling.
     |  transformations: function or list, optional
     |      Provide a list of transformations that you wish to apply to the 
     |      trajectory upon reading. Transformations can be found in 
     |      :mod:`MDAnalysis.transformations`, or can be user-created.
     |  in_memory
     |      After reading in the trajectory, transfer it to an in-memory
     |      representations, which allow for manipulation of coordinates.
     |  in_memory_step
     |      Only read every nth frame into in-memory representation.
     |  continuous : bool, optional
     |      The `continuous` option is used by the
     |      :mod:`ChainReader<MDAnalysis.coordinates.chain>`, which contains the
     |      functionality to treat independent trajectory files as a single virtual
     |      trajectory.
     |  
     |  Attributes
     |  ----------
     |  trajectory
     |      currently loaded trajectory reader;
     |  dimensions
     |      current system dimensions (simulation unit cell, if set in the
     |      trajectory)
     |  atoms, residues, segments
     |      master Groups for each topology level
     |  bonds, angles, dihedrals
     |      master ConnectivityGroups for each connectivity type
     |  
     |  Methods defined here:
     |  
     |  __getattr__(self, key)
     |  
     |  __getstate__(self)
     |  
     |  __init__(self, *args, **kwargs)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  __repr__(self)
     |      Return repr(self).
     |  
     |  __setstate__(self, state)
     |  
     |  add_Residue(self, segment=None, **attrs)
     |      Add a new Residue to this Universe
     |      
     |      New Residues will not contain any Atoms, but can be assigned to Atoms
     |      as per usual.  If the Universe contains multiple segments, this must
     |      be specified as a keyword.
     |      
     |      Parameters
     |      ----------
     |      segment : MDAnalysis.Segment
     |        If there are multiple segments, then the Segment that the new
     |        Residue will belong in must be specified.
     |      attrs : dict
     |        For each Residue attribute, the value for the new Residue must be
     |        specified
     |      
     |      Returns
     |      -------
     |      A reference to the new Residue
     |      
     |      Raises
     |      ------
     |      NoDataError
     |        If any information was missing.  This happens before any changes have
     |        been made, ie the change is rolled back.
     |      
     |      
     |      Example
     |      -------
     |      
     |      Adding a new GLY residue, then placing atoms within it:
     |      
     |      >>> newres = u.add_Residue(segment=u.segments[0], resid=42, resname='GLY')
     |      >>> u.atoms[[1, 2, 3]].residues = newres
     |      >>> u.select_atoms('resname GLY and resid 42')
     |      <AtomGroup with 3 atoms>
     |  
     |  add_Segment(self, **attrs)
     |      Add a new Segment to this Universe
     |      
     |      Parameters
     |      ----------
     |      attrs : dict
     |          For each Segment attribute as a key, give the value in the new
     |          Segment
     |      
     |      Returns
     |      -------
     |      A reference to the new Segment
     |      
     |      Raises
     |      ------
     |      NoDataError
     |          If any attributes were not specified as a keyword.
     |  
     |  add_TopologyAttr(self, topologyattr, values=None)
     |      Add a new topology attribute to the Universe
     |      
     |      Adding a TopologyAttribute to the Universe makes it available to
     |      all AtomGroups etc throughout the Universe.
     |      
     |      Parameters
     |      ----------
     |      topologyattr : TopologyAttr or string
     |        Either a MDAnalysis TopologyAttr object or the name of a possible
     |        topology attribute.
     |      values : np.ndarray, optional
     |        If initiating an attribute from a string, the initial values to
     |        use.  If not supplied, the new TopologyAttribute will have empty
     |        or zero values.
     |      
     |      Example
     |      -------
     |      For example to add bfactors to a Universe:
     |      
     |      >>> u.add_TopologyAttr('bfactors')
     |      >>> u.atoms.bfactors
     |      array([ 0.,  0.,  0., ...,  0.,  0.,  0.])
     |      
     |      .. versionchanged:: 0.17.0
     |         Can now also add TopologyAttrs with a string of the name of the
     |         attribute to add (eg 'charges'), can also supply initial values
     |         using values keyword.
     |  
     |  copy(self)
     |      Return an independent copy of this Universe
     |  
     |  load_new(self, filename, format=None, in_memory=False, **kwargs)
     |      Load coordinates from `filename`.
     |      
     |      The file format of `filename` is autodetected from the file name suffix
     |      or can be explicitly set with the `format` keyword. A sequence of files
     |      can be read as a single virtual trajectory by providing a list of
     |      filenames.
     |      
     |      
     |      Parameters
     |      ----------
     |      filename : str or list
     |          the coordinate file (single frame or trajectory) *or* a list of
     |          filenames, which are read one after another.
     |      format : str or list or object (optional)
     |          provide the file format of the coordinate or trajectory file;
     |          ``None`` guesses it from the file extension. Note that this
     |          keyword has no effect if a list of file names is supplied because
     |          the "chained" reader has to guess the file format for each
     |          individual list member [``None``]. Can also pass a subclass of
     |          :class:`MDAnalysis.coordinates.base.ProtoReader` to define a custom
     |          reader to be used on the trajectory file.
     |      in_memory : bool (optional)
     |          Directly load trajectory into memory with the
     |          :class:`~MDAnalysis.coordinates.memory.MemoryReader`
     |      
     |          .. versionadded:: 0.16.0
     |      
     |      **kwargs : dict
     |          Other kwargs are passed to the trajectory reader (only for
     |          advanced use)
     |      
     |      Returns
     |      -------
     |      universe : Universe
     |      
     |      Raises
     |      ------
     |      TypeError if trajectory format can not be
     |                determined or no appropriate trajectory reader found
     |      
     |      
     |      .. versionchanged:: 0.8
     |         If a list or sequence that is provided for `filename` only contains
     |         a single entry then it is treated as single coordinate file. This
     |         has the consequence that it is not read by the
     |         :class:`~MDAnalysis.coordinates.chain.ChainReader` but directly by
     |         its specialized file format reader, which typically has more
     |         features than the
     |         :class:`~MDAnalysis.coordinates.chain.ChainReader`.
     |      
     |      .. versionchanged:: 0.17.0
     |         Now returns a :class:`Universe` instead of the tuple of file/array
     |         and detected file type.
     |  
     |  make_anchor(self)
     |  
     |  remove_anchor(self)
     |      Remove this Universe from the possible anchor list for unpickling
     |  
     |  select_atoms(self, *args, **kwargs)
     |      Select atoms.
     |      
     |      See Also
     |      --------
     |      :meth:`MDAnalysis.core.groups.AtomGroup.select_atoms`
     |  
     |  transfer_to_memory(self, start=None, stop=None, step=None, verbose=False)
     |      Transfer the trajectory to in memory representation.
     |      
     |      Replaces the current trajectory reader object with one of type
     |      :class:`MDAnalysis.coordinates.memory.MemoryReader` to support in-place
     |      editing of coordinates.
     |      
     |      Parameters
     |      ----------
     |      start: int, optional
     |          start reading from the nth frame.
     |      stop: int, optional
     |          read upto and excluding the nth frame.
     |      step : int, optional
     |          Read in every nth frame. [1]
     |      verbose : bool, optional
     |          Will print the progress of loading trajectory to memory, if
     |          set to True. Default value is False.
     |      
     |      
     |      .. versionadded:: 0.16.0
     |  
     |  ----------------------------------------------------------------------
     |  Class methods defined here:
     |  
     |  empty(n_atoms, n_residues=None, n_segments=None, atom_resindex=None, residue_segindex=None, trajectory=False, velocities=False, forces=False) from builtins.type
     |      Create a blank Universe
     |      
     |      Useful for building a Universe without requiring existing files,
     |      for example for system building.
     |      
     |      If `trajectory` is set to True, a
     |      :class:`MDAnalysis.coordinates.memory.MemoryReader` will be
     |      attached to the Universe.
     |      
     |      Parameters
     |      ----------
     |      n_atoms : int
     |        number of Atoms in the Universe
     |      n_residues : int, optional
     |        number of Residues in the Universe, defaults to 1
     |      n_segments : int, optional
     |        number of Segments in the Universe, defaults to 1
     |      atom_resindex : array like, optional
     |        mapping of atoms to residues, e.g. with 6 atoms,
     |        `atom_resindex=[0, 0, 1, 1, 2, 2]` would put 2 atoms
     |        into each of 3 residues.
     |      residue_segindex : array like, optional
     |        mapping of residues to segments
     |      trajectory : bool, optional
     |        if True, attaches a :class:`MDAnalysis.coordinates.memory.MemoryReader`
     |        allowing coordinates to be set and written.  Default is False
     |      velocities : bool, optional
     |        include velocities in the :class:`MDAnalysis.coordinates.memory.MemoryReader`
     |      forces : bool, optional
     |        include forces in the :class:`MDAnalysis.coordinates.memory.MemoryReader`
     |      
     |      Returns
     |      -------
     |      MDAnalysis.Universe object
     |      
     |      Examples
     |      --------
     |      For example to create a new Universe with 6 atoms in 2 residues, with
     |      positions for the atoms and a mass attribute:
     |      
     |      >>> u = mda.Universe.empty(6, 2,
     |                                 atom_resindex=np.array([0, 0, 0, 1, 1, 1]),
     |                                 trajectory=True,
     |              )
     |      >>> u.add_TopologyAttr('masses')
     |      
     |      .. versionadded:: 0.17.0
     |      .. versionchanged:: 0.19.0
     |         The attached Reader when trajectory=True is now a MemoryReader
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors defined here:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  anchor_name
     |  
     |  angles
     |      Angles between atoms
     |  
     |  bonds
     |      Bonds between atoms
     |  
     |  coord
     |      Reference to current timestep and coordinates of universe.
     |      
     |      The raw trajectory coordinates are :attr:`Universe.coord.positions`,
     |      represented as a :class:`numpy.float32` array.
     |      
     |      Because :attr:`coord` is a reference to a
     |      :class:`~MDAnalysis.coordinates.base.Timestep`, it changes its contents
     |      while one is stepping through the trajectory.
     |      
     |      .. Note::
     |      
     |         In order to access the coordinates it is better to use the
     |         :meth:`AtomGroup.positions` method; for instance, all coordinates of
     |         the Universe as a numpy array: :meth:`Universe.atoms.positions`.
     |  
     |  dihedrals
     |      Dihedral angles between atoms
     |  
     |  dimensions
     |      Current dimensions of the unitcell
     |  
     |  impropers
     |      Improper dihedral angles between atoms
     |  
     |  is_anchor
     |      Is this Universe an anchoring for unpickling AtomGroups
     |  
     |  kwargs
     |      keyword arguments used to initialize this universe
     |  
     |  trajectory
     |      Reference to trajectory reader object containing trajectory data.
     |  
     |  universe
    



```python
u.trajectory.n_frames
```




    98




```python
u.atoms
```




    <AtomGroup with 3341 atoms>




```python
u.atoms.dimensions
```




    array([ 0.,  0.,  0., 90., 90., 90.], dtype=float32)



MDAnalysis is strongly structured. Attributes of MDAnalysis Classes can be accessed by typing <\__dict__> after the object we are interesting in.

## SELECTION
- same format of VMD selection commands
- atom selections are different objects, **AtomGroup**


```python
alanines = u.select_atoms('resname ALA')
```


```python
alanines.resids
```




    array([  8,   8,   8,   8,   8,   8,   8,   8,   8,   8,  11,  11,  11,
            11,  11,  11,  11,  11,  11,  11,  17,  17,  17,  17,  17,  17,
            17,  17,  17,  17,  37,  37,  37,  37,  37,  37,  37,  37,  37,
            37,  38,  38,  38,  38,  38,  38,  38,  38,  38,  38,  49,  49,
            49,  49,  49,  49,  49,  49,  49,  49,  55,  55,  55,  55,  55,
            55,  55,  55,  55,  55,  66,  66,  66,  66,  66,  66,  66,  66,
            66,  66,  73,  73,  73,  73,  73,  73,  73,  73,  73,  73,  93,
            93,  93,  93,  93,  93,  93,  93,  93,  93,  95,  95,  95,  95,
            95,  95,  95,  95,  95,  95,  99,  99,  99,  99,  99,  99,  99,
            99,  99,  99, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127,
           176, 176, 176, 176, 176, 176, 176, 176, 176, 176, 186, 186, 186,
           186, 186, 186, 186, 186, 186, 186, 188, 188, 188, 188, 188, 188,
           188, 188, 188, 188, 194, 194, 194, 194, 194, 194, 194, 194, 194,
           194, 203, 203, 203, 203, 203, 203, 203, 203, 203, 203, 207, 207,
           207, 207, 207, 207, 207, 207, 207, 207])




```python
np.unique(alanines.resids)
```




    array([  8,  11,  17,  37,  38,  49,  55,  66,  73,  93,  95,  99, 127,
           176, 186, 188, 194, 203, 207])




```python
alanines.residues
```




    <ResidueGroup with 19 residues>



### many types of selection commands (as usual)
+ using wildcards
+ using types
+ using index
+ etc.


```python
carbons = u.select_atoms('name C*')
carbons.names
```




    array(['CA', 'CB', 'CG', ..., 'C', 'C', 'CA'], dtype=object)




```python
carbons[:10].names
```




    array(['CA', 'CB', 'CG', 'CE', 'C', 'CA', 'CB', 'CG', 'CD', 'CZ'],
          dtype=object)



## CALCULATIONS
 - RMSD: deviations of a subset of the atoms of the structure between two different frames of the trajectory
 - useful measure of structure similarity


```python
from MDAnalysis.analysis.rms import rmsd
```


```python
np.shape(u.select_atoms("name CA").positions)
```




    (214, 3)




```python
ref = mda.Universe(str(PSF), str(PDB))
```


```python
rmsd(u.select_atoms('name CA').positions, ref.select_atoms('name CA').positions)
```




    28.204258162856345




```python
rmsd_WRONG = [rmsd(u.select_atoms('name CA').positions, ref.select_atoms('name CA').positions) for ts in u.trajectory[:]]
```


```python
import seaborn as sns
```


```python
plt.plot(rmsd_WRONG)
plt.xlabel('time')
plt.ylabel('RMSD')
plt.title('Wrong RMSD')
```




    Text(0.5, 1.0, 'Wrong RMSD')




![png](output_30_1.png)


### let's remove translations and rotations



```python
rmsd_TRUE = [rmsd(u.select_atoms('name CA').positions, ref.select_atoms('name CA').positions, center=True, superposition=True) for ts in u.trajectory[:]]
```


```python
plt.plot(rmsd_TRUE)
plt.xlabel('Time')
plt.ylabel('RMSD')
plt.title('True RMSD')
```




    Text(0.5, 1.0, 'True RMSD')




![png](output_33_1.png)


# overall map of rmsd between structures
#### use with care, it may be computationally expensive

- let's copy the Universe
we want a **NEW** object equal to universe u, not a **NEW VARIABLE** that points to the same object


```python
u_new = u.copy()
```

- sometimes it may be useful to reduce the size of samples to avoid computational problems (if you have many timesteps)
- this is not the case, but let's see how to perform this reduction quickly


```python
reduced_index = list(range(0,u.trajectory.n_frames,2)) # try to change this number
print(reduced_index)
```

    [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 96]



```python
rmsd_map = [rmsd(u.select_atoms('name CA').positions, u_new.select_atoms('name CA').positions, center=True, superposition=True) for ts in u.trajectory[reduced_index] for ts_one in u_new.trajectory[reduced_index]]
```

- let's draw a fancy plot


```python
#print(rmsd_map)
rmsd_map = np.array(rmsd_map).reshape((len(reduced_index),len(reduced_index)))
plt.figure(figsize=((10,10)))
plt.title("RMSD heatmap", fontsize=20)
img = plt.imshow(rmsd_map)
plt.colorbar(img);
```


![png](output_41_0.png)


## RMSF
- measures the fluctuations of each atom with respect to the equlibrium
- proportional to the temperature ($\beta$) factor
- useful to match experimental results


```python
from MDAnalysis.analysis.rms import RMSF
```


```python
ca_atoms = u.select_atoms("protein and name CA")
myRMSF = RMSF(ca_atoms).run()
```


```python
plt.plot(myRMSF.rmsf)
```




    [<matplotlib.lines.Line2D at 0x7fcfbcd4a5c0>]




![png](output_45_1.png)


## **EXERCISE**
- does the previous plot look reasonable?
- try to compare it to the "*real*" distribution of experimentally observed temperature factors
- *Hint*: you can access the actual $\beta$ factors from a universe: *Universe._topology.tempfactors*


```python
#SOLUTION:
print("TODO: implement me!")
```

    TODO: implement me!


## CONTACT MAP
- C_IJ = 1 if d_ij < cutoff
- measures the closeness of residues


```python
from MDAnalysis.lib import distances
```


```python
Ca = ref.select_atoms('name CA')
d_CaCa = distances.distance_array(Ca.positions, Ca.positions)
```


```python
#fig, ax = plt.subplots()
plt.figure(figsize=((10,10)))
plt.title("Contact map", fontsize=20)
img = plt.imshow(d_CaCa)
plt.colorbar(img);
```


![png](output_51_0.png)



```python

```
