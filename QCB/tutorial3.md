---
layout: page
author: Tubiana Luca
mathjax: true
---
<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>

## Setting up a collective project &  scripting analyses.
In this lecture we will see how to setup properly a project based on Molecular
Dynamics simulations of a biophysical system. This topic will be covered in
more details during the other lectures, but we start introducing it here.

This lecture will cover:  
* Presentation of the projects covering part of your final exam.
* How a computational biophysics project should be organized.
* Setting up the working directory properly.
* Scripting with TCL and VMD to analyze results.
* Analysis with MDAnalysis.

## The goal
Part of the final exam will consist in a simulation of mutants of _recoverin_, a protein 
involved in photoreception in the vertebrates eyes. Recoverin plays an important role in 
phototrasnduction, the main mechanism behind vision in mammals.

### Human vision
{:refdef: style="text-align: center;"}
![eye w enlarged retina](../../img/tut3/cross_section_eye_w_retina_T_Delbruck_thesis.png)  
_image adapted from Tobi Delbruck's PhD Thesis ["Analog And Digital Implementations Of Retinal Processing For Robot Navigation Systems"](https://www.researchgate.net/publication/324890076_Analog_And_Digital_Implementations_Of_Retinal_Processing_For_Robot_Navigation_Systems)_
{:refdef}

The retina has 3 known types of photoreceptor cells: rods (light sensitivity),
cones (color perception) and photosensitive ganglion cells (mainly regulatory
functions). These cells transduce light into different membrane voltages, thus
activating a neuronal signal which finally reaches the brain.

### The role of recoverin
Recoverin is a 23 kDa Calcium binding protein and a prototypical of the
neuronal calcium sensor family mostly expressed in vertebrate photoreceptors,
where it plays a role in the regulation of the phototransduction cascade.
Light triggers a transient drop in the intracellular concentration of Ca 2+ and
such change is promptly detected by a group of calcium sensors including Rec,
which **reversibly bind to specific targets in a Ca 2+ -dependent manner**.
Such specific binding permits the complex regulation of the signaling cascade
and is fundamental to allow vision in low-light condition, thanks to its
interaction with rhodopsin kinase.

### How does the binding happen?
{:refdef: style="text-align: center;"}
![myristoyl insertion](../../img/tut3/GL_mirystoyl_insertion.png)
_Dynamics of myristoyl insertion into the membrane for Rec-GRK1. Rec (green) and GRK1
peptide (orange) structures are shown as cartoons, Ca 2+ ions are represented as red spheres,
N-terminal myristoyl group is shown as purple spheres, upper leaflet of the membrane is
represented as light grey spheres, PS lipids and Rec residues involved in membrane anchoring are
shown as sticks and colored according to chemical elements: O atoms are red, N atoms are blue, P
atoms are orange, C atoms are green for Rec and yellow for PS. A, B, C: specific steps of insertion
referring to the time in the label; D, E F: zoomed-in view of A, B and C, respectively.
(Image adapted from Borsatto A, Marino V, Abrusci G, Lattanzi G and Dell'Orco D,
 "Effects of membrane and biological target on the structural and allosteric properties
of recoverin, a myristoyl-switch protein", Int. Jour. Mol. Sci. 20 (2019 - in press))_ 
{:refdef}

### Can different mutations in the protein influence the binding?
**We want to know!** And this is in fact the goal of the projects.
* 4 groups.
* Each group will work on one mutation.
* Each group will have to work out the following steps:
  - Equilibrate the mutated protein in water
  - Verify the mutated protein is stable
  - Study the stability of the mutant attached to the membrane

<p class="prompt prompt-attention"> This is a **research project.** Those of
you who want, and who manage to produce the steps above properly, **will be
included as authors in the resulting article**.</p>

<p class="prompt prompt-question"> How shall we proceed to set up a collaborative research project?? </p>

## How a CBP project should be organized or, 'the three laws of CBP'.
Before proceeding further, let's discuss a bit how to set up a project so
that it is **reproducible**, **efficient**, and **scalable**. Think of these 
as if they where the three laws of ~~robotics~~ CBP.  

1. The results of a research project must be **reproducible**.   
2. The protocol leading to the results should be as **efficient**, as long as this
  efficiency does not hinder reproducibility.   
3. The results of a research project should produce an impact at **scale**, as
   long as this does not conflict with the reproducibility and efficiency. 

### Script & Automate everything you can.
Computers and automation exist so that we can spend less time doing repetitive
tasks.  Those vary from vacuuming the floor to solving difficult equations to
simulating entire systems (_efficiency_).

Another advantage of automating tasks is that -if done properly- you know
exactly how they are executed (_reproducibility_). 

If you do not try to reinvent the wheel, build consistently on top of
existing software, and frequently discuss techniques and aims with peers your 
research and protocol will spread more easily (_scalability_).

<p class="prompt prompt-attention"> Think carefully about what is an efficient approach....</p>

<p class="prompt prompt-question"> Producing plots very quickly by hand, saving things in random directories? </p>

<p class="prompt prompt-question"> Spending a lot of time designing your scripts & protocols? </p>


When you think about designing a system, keep in mind that you want to optimize the overall time 
required to get a **reproducible** result...

$$T = T_{run} + T_{develop} + T_{maintainence}$$


...And perhaps, **do not overdo it!** 
![efficient lab](../../img/tut3/tomgauld_robots.jpg)  

### Choose a proper folder structure
An important and often overlooked aspect of running a project is that if you
design things correctly you will considerably reduce the amount of work
required to understand and recover things.

A project folder tends to explode into chaos pretty quickly...
```bash
├── sim_0_eq
│   ├── in.namd
│   ├── rmsd.dat
│   ├── trj.coor
│   ├── trj.dcd
│   └── trj.xsc
├── sim_1_min
│   ├── in.namd
│   ├── energia.dat
│   ├── rmsd_1.dat
│   .....
│   ├── trj.dcd
│   └── trj.xsc
├── sim_2_equi_vero
│   ├── in_equi3.namd
│   ├── scripts
│   │   └── analisi.py
│   ├── energia.dat
│   ├── rmsd_1.dat
│   ├── dihedral.dat
│   .....
│   ├── trj.dcd
│   └── trj.xsc
....
├── sim_10_valar_morghulis
│   ├── valar_dohaeris.namd
│   ├── scripts
│   │   ├── holdthedooor.py
│   │   ├── holdthedooor.py
│   │   └── analisi3.py
│   ├── energia.dat
│   ├── rmsd_1.dat
│   ├── dihedral.dat
│   ├── holdthedoor.dat
│   .....
│   ├── trj.dcd
│   └── trj.xsc
├── analysis
│   ├── energiaaaaa.dat
│   ├── rmsd_1.dat
│   ├── rmsd_volta_buona.dat
│   ├── analisi_finale.dat
│   ├── analisi_finale2.dat
│   ├── analisi_finale2b.dat
│   ├── valar_morghulis.dat
│   └── valar_morghulis.py
├── report
│   ├── img
│   │   ├── plot_1a.png
│   │   ├── plot_1b.png
│   │   ├── plot_2a.png
│   │   ├── plot_2b.png
│   │   ....
│   │   ├── plot_22b_holdthedoor.png
│   │   ....
│   │   ├── plot_34_usa_questo.png
│   │   └── plot_34_usa_questo2.png
│   ├── report_v1.tex
│   ├── report_v2.tex
│   ├── report_v2a.tex
│   ├── report_v2b.tex
│   ├── report_v3a.tex
│   ├── report_v3b.tex
│   ├── report_v3c.tex
│   .....
│   ├── report_finaleA.tex
│   ├── report_finaleB.tex
│   └── report_finaleC.tex
....

```

In some fortunate vase, years of frustration cristallize on top of successful
codes in the form of dark humour mocking the user, but since it is not always
the case, so **let's try to think of a decent structure**. 

<p class="prompt prompt-attention">We will improve upon this, and if you have comments during the course, they are welcome!</p>

```bash
project_name
├── 00_bin
│   ├── create_env.sh
│   ├── create.sh
│   └── params.sh
├── 01_sim
│   ├── G01_scripts
│   └── G01_sim_0
├── 02_EDA_notebooks
│   └── G01
├── 03_prod_analysis
│   ├── G01_data_0
│   └── G01_scripts
├── 04_report
│   ├── imgs
│   └── report_template.tex
├── conda_QCB_essentials.yml
└── README.md

```

You can donwload this folder structure plus the conda python environment [here](../materials/md-proj-template.zip).

* `00_bin` contains scripts to build the directory structure. Use an editor to
  set an `id` in `00_bin/params.sh` and then run `create.sh` to create the subfolders for a group.
* `01_sim` will contain the scripts used to generate the trajectories, the
  parameter files for the trajectories, and the simulations results.
* `02_EDA_notebooks` is intended to store jupyter notebooks for exploratory
  data analysis.
* `03_prod_analysis` contains a subfolder `scripts` in which to store analysis
  scripts working on trajectory data, and one or more subfolders with the result of each scripted analysis.
* `04_report` will contain the report of your project. The img folder should
  contain a subfolder for each image. This subfolder should contain the image
and a small file explaining what script produced it, on which data.
* `conda_QCB_essentials.yml` contains the requirements for the conda python environment.


## Scripting: TCL + VMD

### Learning to fly...
{:refdef: style="text-align: center;"}
![the proper way](../../img/tut3/schneider_schoolglider_b.png)
_in the 30s planes and gasoline were expensive. Pilots were trained by putting them on a 
small, single-seat glider like the Schneider SG-38 above and [slingshotting them from the top of a hill](https://en.wikipedia.org/wiki/DFS_SG_38_Schulgleiter).   
You get were this is going._ 
{:refdef}

<p class="prompt prompt-attention"> **Assignment**</p>
Prepare a different TCL script to be run by vmd for each of the following ananalyses:
1. Print the RMSD with respect to the initial frame.
2. Print all dihedrals of a protein trajectory.
3. Print the solvent accessible surface (sasa) for your protein.

<p class="prompt prompt-question"> **Requirements** </p>
* Your script should work on any trajectory, without the need to modify it. 
* The output should go to a file. 
* The output filename must _not_ be hardcoded. 
<p class="prompt prompt-info"> You can use google. </p>
<p class="prompt prompt-info"> You can ask questions to me or Marco. </p>

You can test your scripts first on the alanine dipeptide trajectory from last
lecture; once you are happy, use it to analyze the trajectory contained in
[this archive](../materials/tutorial3.tar.gz)

Before that, let us recap what you need to know to do the task.

### VMD and TCL
**First** of all, the basis of oyur analysis will be VMD. VMD can run in "batch mode",
that is, without launching the gui and without interacting with the user. How?
#### Launching VMD in batch mode:
```bash
vmd -dispdev text -eofexit -args arg1 arg2 ... argN < script.tcl
```
* `-dispdev text`: set display to text only (do not open gui).
* `-eofexit`: vmd will run in batch mode and exit once `script.tcl` reaches its last line.  
* `-args ...`: gives a way to pass arguments to script.tcl

**Second**, VMD has several functions already available to let you analyses
proteins. It is actually pretty powerful. The most useful ones for analysis are:
* `mol`: create new molecules and activate them. returns the molecule id.
* `molinfo`: returns several info on the molecule, like frame numbers for a traj.
* `atomselect`: let user select different sets of atoms, and perform simple measure on them.
* `measure`: perform more complex measures (rmsd, rgyr, sasa, charges..) on a selection.
Aside for those, remember that any element in a VMD menu correspond to a TCL 
package in your VMD distribution. And that code is available.

**Third**, The main commands in VMD are self documenting. If you type
`atomselect` in the tk console and hit enter, you will get a small description
of how to use it.
```tcl
vmd > atomselect
usage: atomselect <command> [args...]

Creating an Atom Selection:
  <molId> <selection text> [frame <n>]  -- creates an atom selection function
  list                         -- list existing atom selection functions
  (type an atomselection function to see a list of commands for it)

Getting Info about Keywords:
  keywords                     -- keywords for selection's get/set commands
  symboltable                  -- list keyword function and return types

Atom Selection Text Macros:
  macro <name> <definition>    -- define a new text macro
  delmacro <name>              -- delete a text macro definition
  macro [<name>]               -- list all (or named) text macros
```


<p class="prompt prompt-info"> When you use VMD with the gui, **you can log all TCL commands to a log-file**.
See the 'file' menu in the main window... </p>

#### Finding help on VMD scripting
* [VMD reference manual](https://www.ks.uiuc.edu/Research/vmd/current/ug/ug.html)
* [VMD tutorial](https://www.ks.uiuc.edu/Training/Tutorials/vmd/tutorial-html/)
* [VMD plugin programmer's guide (advanced)](https://www.ks.uiuc.edu/Research/vmd/plugins/doxygen/) 

### Deep dive in TCL
{:refdef: style="text-align: center;"}
![deep-diving](../img/tut3/deep-diver.jpg){:heigth="40%" width="40%"}  
_If you stare into TCL, TCL stares back at you 
(beyond [good](https://www.butterfly.com.au/blog/website-development/clean-high-quality-code-a-guide-on-how-to-become-a-better-programmer)
 and [evil](https://gist.github.com/aras-p/6224951) code)_
{:refdef}
####  Variables
You assign a value to a variable with `set`, and reference it with `$`. Variables in TCL
can store basically anything (including whole functions).

```tcl 
# This is a comment
set var value ;# this is another comment
set var "value1 value2 value3"  ;# assign a list to var
set var {value1 value2 value3}  ;# assign a list to var
```
`puts` stands for "put string", it prints a string. We will see it in a little
more details later on.

```tcl
set var mondo
> mondo
puts "ciao, $var"
> ciao, mondo
puts {ciao, $var}
> ciao, $var
puts ciao mondo
> can not find channel named "ciao"
```
we will see the reason behind  this error later.

#### Command substitution 
TCL evaluates all commands between two square brackets first. Their output can
be assigned to a variable.

By the way: `expr` evaluates a mathematical expression, including logical
operators. The supported operators and functions can be found [here](https://www.tcl.tk/man/tcl8.6/TclCmd/expr.htm).
```tcl
set a 10
set b 20
set op *
set result [expr $a $op $b]
>200
set op /
set result [expr $result $op $a]
>20
```

This works of course also with VMD commands, and in fact with any valid TCL script.
```tcl
set mymol [mol new 4AKE.pdb]
set atname "CA"
set mysel [atomselect $mymol "$atname"]
```


#### lists 
Lists in TCL are associative, kind of like in python. No slicing unfortunately..
```tcl
#building a list
set list1 "1 2 3 4"
> 1 2 3 4 
set list2 "$list1 5 6"
> 1 2 3 4 5 6
set list3 "{$list1} 5 6"
> {1 2 3 4} 5 6
#get length
llength $list1
>4
#get elements
lindex $list1 0
> 1
lindex $list3 0 1
> 2
```

#### Control structures
We wil cover just the very basic: `for`, `foreach` and `if`.
```tcl
set list1 "1 2 3 4 5"
foreach el $list1 { puts [expr 10*$el] }
#or, using a for loop
set n [llength $list1]
for {set i 0}{$i<$n}{incr i} { 
  puts [expr 10*[lindex $list1 $i]] 
}
```

If follows the construct:
```tcl
if { $condition} { 
# statement here 
} elseif { $condition2} { 
# other statement 
} else {
# default statement
}
```
#### Procs
Of course a proper scripting language needs functions. These in TCL are a way
to create new commands. You can find more details on procs [here](https://wiki.tcl-lang.org/page/TCL+for+beginners#94a18b74d5f51af8db7cb0492b556456a309b8ab9316f7033d799497f1165d4c) and [here](https://wiki.tcl-lang.org/page/proc).

You can create and use a proc as follow. Note the default value set for arg3.
As a consequence we can call `my_proc` with only 2 arguments.

Note: variables defined inside procs are not visible outside, and viceversa.
```tcl
#ALWAYS write a docstring before the proc, explaining:
#1. what your proc does.
#2. The parameters in input and their expected type.
#3. The result returned.
proc my_proc {arg1 arg2 {arg3 10}} {
  set internal_var [expr $arg3*($arg1 + $arg2)/2]
  return $internal_var
}
my_proc 2 2 
> 10
my_proc 2 2 20
> 20
```

Procedures can also take a variable number of arguments. In this case you need to call them
using the special argument `args`:
```tcl 
proc my_proc {arg1 args} {
  #args is now a list!
  set n [[length args]]
  if {$n == 0 } {
    error " I am expecting a list!"
   }
   ...
}
```
  

#### Opening and closing files.
```tcl
set fout [open "file_out.txt" w]
set fin [open "file_in.txt" r]
close $fout
close $fin
```

#### Puts & format
`puts` actually takes two arguments, only the first defaults to `stdout`. This
is where puts will print the string passed to it. One can of course specify 
something different, for example a file.
```tcl
set fout [open "file_output.txt" w] ;# open 'file_output.txt' in write mode.
puts $fout "Ciao, mondo" ;# writes 'Ciao, mondo' to file_output.txt
close $fout ;# closes the stream to file_output.txt
```

When writing an output, you might want to format it properly. For this, use
`format`.

```tcl
set x 10.124124
set y 4.120708
format "%5.2f %5.2f $x $y
>10.12  4.12
```
`format` uses a convention to print numbers and strings. You can specify the type of 
number (integer, floating point) and the amount of digits, plus alignment etc. More
details can be found [here]().

#### Running a script
A TCL script reads arguments through a list.
* `argc`: number of arguments passed to the script.
* `argv`: list of arguments passed to the script.
* `argv0`: name of the script.

An example: a script that collates two words passed to it.
```tcl
#!/bin/tclsh
set script $argv0
set nargs $argc
set wordlist $argv
foreach w $wordlist {puts  -nonewline [format "%s" $w]}
puts " " ;# add newline

puts stderr "$script collated $n words."
```
Usage:
```bash
$ collate.tcl trenta tre trentini entrarono a trento
$> trentatretrentinientraronoatrento
$> collate.tcl collated 6 words.
```


## Part 2. Python

In this second part we will see how to use a python package,
[MDAnalysis](https://www.mdanalysis.org/) to analyze MD trajectories. This part
will be explained directly on a jupyter notebook, which you can find
[here](../materials/mdanalysis_notebook.ipynb). The data needed by the notebook
is, as for the first part, [in this .tar archive](../materials/tutorial3.tar.gz)

<p class="prompt prompt-attention"> **Requirements**</p>
You will need to download and install [miniconda](https://docs.conda.io/en/latest/miniconda.html).
The necessary packages to run the jupyter notebook are in the conda environment file 
you found in the template directory.



