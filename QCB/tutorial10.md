---
layout: page
authors: [Luca Tubiana, Marco Giulini]
mathjax: true
---

<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  tex2jax: {
    inlineMath: [['$','$'], ['\\(','\\)']],
    processEscapes: true
  }
});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>

##Graphs, proteins, pyinteraph web servers for computational biophysics
In this lecture:
1. A quick intro to graph theory.
2. Pyinteraph
3. Setting up Skynet to run pyinteraph
4. Webservers
   - Benchmarks
   - Elastic Network Models

## A quick to graph theory. 
Graph theory is a rich and interesting branch of mathematics with applications ranging from electrical
circuits, to topology, to network optimizations and analysis of protein structures.

We will cover only some basic notions of graph theory, in order to better understand
pyinteraph and other research papers (and softwares) applying graph and network
concepts to proteins' structures.

### Definition
A **graph** $ G=G(V;E) $ consists of a set of vertices V and a set of edges E.
Two vertices $v_i$ and $ v_j $ are said to be adjacent if there is an edge
$e_{if}$ connecting them. In the same way, two edges $e_{ij}$ $e_{jk}$ are
said to be adjacent if they have at least a vertex in common.

{:refdef: style="text-align: center;"}
![graph definitions](../img/tut10/tut10_notes_GT-1.png)  
_Basic concepts from graph theory. a) Schematic representations of vertices 
and edges. b) A digraph, or directed graph. Those are most often used to 
represent electric circuits. c) Weigthed graphs have different numerical value 
(cliques) each vertex is connected to all other vertices._
{:refdef}  

There are several possible things one can consider in graphs. One can put
arrows on the edges to indicate a flow, as in electric networks or chemical
reactions, one can consider multiple edges between vertices (_multigraphs_), or
give different **weights** to different vertices and edges. This latter
consideration is particularly important in Physics (and Chemistry!), where we
want not only to know that there exists a connection between two entities, but
also its strength or probability. For example, graph c) could indicate a
molecule: the vertices are the atoms and the edges the bonds. Their respective
weigths could be set to indicate different chemical properties.


From our definitions of a graph, it follows immediately that any graph $G(V;E)$
has subgraphs, $\\{ G'(V';E')\qquad  |\qquad  V'\subseteq V,\quad E'\subseteq E \\}$.
{:refdef: style="text-align: center;"}
![graph definitions](../img/tut10/tut10_notes_GT-2.png)
_a) An example graph and, b) some of its subgraphs._

<p class="prompt prompt-question">Roughly, how many different subgraphs can be identified in a given graph?</p>


For any two vertices $v_i$ and $v_j$ in a graph, we say that there is a **path** connecting them 
if there exist a sequence adjacent edges $\\{e_{il}e_{la}e_{ab}\ldots e_{mp}e_{pj}\\}$.
Of course these is not necessary, and two vertices or even two portions of a graph 
can be **disconnected**, i.e. not joined by any path.  A (connected) component of $G$ is a 
subgraph $G'$ of $G$ such that all of its vertices are connected by at least a path and there 
are no paths between any of them and another component of $G$. Components are frequently 
referred to as **clusters** in physics.
{:refdef: style="text-align: center;"}
![graph definitions](../img/tut10/tut10_notes_GT-4.png)
_Graphs with different connected components. In both cases the diameter of the
largest component is highlighted in yellow. In b) the removal of a single edge
(a "bridge") causes the appearance of a third component._
{:refdef}  

By removing edges it is possible to split a graph in a set of connected components. This is 
particularly relevant in the applications related to protein structures and analysis, where the
graphs are weigthed since they represent some interactions. Introducing a threshold then splits 
up the graph into different connected components based on the value of the threshold.

<p class="prompt prompt-question">Can you give an every-day life example of a
weighted network which gets splitted in different components due to a
threshold?</p>

### Some simple properties
There are several measures that can be defined for graphs. We will limit 
ourselves to the simplest ones:, **degree** of a vertex, **distance** between two vertices,
 **eccentricity** of a vertex, **center** of a graph.

The **degree** of a vertex, $D(v)$ is simply the number of edges incident to
it. Vertices with a very high degree are often called 'hubs' and they are very
important when studying the statistical properties of networks.

The **distance** between two vertices $v_i$ and $v_j$ is the length (number of edges) of
the shortest path connecting $v_i$ and $v_j$, if it exists, or $\infty$ if $v_i$ and $v_j$
are disconnected.

Given a graph $G(V;E)$, the **Eccentricity** of a vertex $v_i$ is defined as
$$E(v_i) = max_{v_j \in V} d(v_i,v_j).$$

The maximum eccentricity is the **diameter** of the graph.
$$Diam(G) = max_{(v_i,v_j) \in V} d(v_i,v_j).$$

The vertex $v_c$ for with minimal eccentricity is defined to be the **center** of the graph.
{:refdef: style="text-align: center;"}
![graph definitions](../img/tut10/tut10_notes_GT-3.png)
_a) A path between two vertices. b) Distance between the same two vertices. 
c) Eccentricity of the graph.  d) Center of the graph._
{:refdef}  

### Isomorphism
Graphs are **topological objects**. This means that the way we draw the diagram
does not change the graph, which is identified just by a set, $V$, of vertices
and a set, $E$, of edges connecting (some of) them. 

Two graphs with the same diagram, but different labels are considered distinct. 
Nonetheless, it is clear that there must be a relation between them. This relation
is an isomorphism. Two graphs $G_\alpha(V;E)$ and $G_\beta(W;F)$ are said to be 
**isomorphic** is there exist a bijective function between $(V;E)$ and $(W;F)$ 
so that the structure of the graphs is maintained. 

In the figure below, the graphs a) and b) are isomorphic. While the graphs c) 
and d) are not.
{:refdef: style="text-align: center;"}
![graph definitions](../img/tut10/tut10_notes_GT-5.png)
_a) and b) are isomorphic. c) and d) are not._
{:refdef}  

### Mapping graphs to matrices
Graphs can be mapped easily to matrices. One of the most used is the **Adjacency matrix**,
whose elements are defined as in the figure below.  This matrix is often called contact matrix in protein physics.
{:refdef: style="text-align: center;"}
![graph definitions](../img/tut10/tut10_notes_GT-6.png)
_Two graphs and their corresponding adjacency matrices._
{:refdef}  

Adjacency matrices are not the only one relevant in graph theory, but they
highlight an important principle: for undirected graphs, the matrices are
symmetric. This means that they can be diagonalized, and their spectra hold
information about the graph which is independent of the labelling of the
vertices. Among other things, spectral analysis is used to identify isomorphic
graphs and subgraphs, something very useful in the analysis of protein
structure and comparison of different protein folds.


## Pyinteraph
[Pyinteraph](http://www.academia.edu/download/45211058/PyInteraph_a_framework_for_the_analysis_20160429-8155-16961gb.pdf) 
is a software, written in python and cython which identifies **Interaction networks**,
i.e. graphs in which the vertices are atoms or amino-acids and the
edges are different types of interactions. The graphs identified are weigthed,
with each edge proportional to the persistence of a given interaction in an
thermodynamic ensemble.

Pyinteraph considers three type of interactions: **Hydrophobic**, **Salt
bridges**, and  **H-Bonds**. They are identified according to the following
rules.

The details below are taken from the original article.

### Hydrophobic contacts
For hydrophobic contacts, the interaction between two residues is included if
the center of mass of the side chain of the two hydrophobic residues is found
within 5 Å of distance as a default.
{:refdef: style="text-align: center;"}
![Hydrophobic interaction](../img/tut10/pyinteraph_hydrophobic_network.png)
_(Image adapted from: Tiberti, M., et al (2014). Jour. chem. inf. mod., 54(5), 1537-1551.) Hydrophobic interactions network._
{:refdef}  

### Salt bridges
For salt bridges, all the distances between atom pairs belonging to two
“charged groups” of two diﬀerent residues are calculated, and the charged
groups are considered as interacting if at least one pair of atoms is found at
a distance shorter than 4.5 Å
{:refdef: style="text-align: center;"}
![Salt bridges](../img/tut10/pyinteraph_salt_bridges_network.png)
_(Image adapted from: Tiberti, M., et al (2014). Jour. chem. inf. mod., 54(5), 1537-1551.) Salt bridges network. Edges thickness is proportional to their strength (or "persistence"), hubs are colored in yellow._
{:refdef}  

### H-Bonds
A H-bond is identiﬁed when both the distance between the acceptor atom and the
hydrogen atom is lower than 3.5 Å and the donor-hydrogen-acceptor atom angle is
greater than 120°. These default parameters can be modiﬁed by the user. As a
default, both side chain and main chain H-bonds are included
{:refdef: style="text-align: center;"}
![Hydrogen bonds](../img/tut10/pyinteraph_HB_network.png)
_(Image adapted from: Tiberti, M., et al (2014). Jour. chem. inf. mod., 54(5), 1537-1551.) Hydrogen bonds network. Bond thickness is proportional to their weight ("persistence"); a set of very persistent ones are 
reported in the right panel._
{:refdef}  

### Pyinteraph workflow
How does pyinteraph identify the various interaction networks? It works as schematically reported below:
{:refdef: style="text-align: center;"}
![pyinteraph scheme](../img/tut10/pyinteraph_scheme.png)
_(Image adapted from: Tiberti, M., et al (2014). Jour. chem. inf. mod., 54(5), 1537-1551.) How pyinteraph works: 0. Start from an MD trajectory. 1. Identify weigthed adjacency matrices, 2. filter them, 3. join them into an unweighted matrix_
{:refdef}  
Starting from an MD trajectory, pyinteraph computes for every frame the hydrophobic, salt bridges and H-bond based on the relative distance and orientation of the relevant atoms. The output from this first step consist of three weighted graphs (i.e. three matrices), one per interaction, in which the weight of the edges correspond to the fraction of frames in which said bonds were active.  The program (or the user) then identifies a critical persistence length to separate clusters which are persistent from interactions which are rarer. There is usually a sharp transition, as shown in the image below. As a second step, the program outputs three filtered matrices. Finally, it puts them together as an unweigthed matrix representing the whole interaction network of the protein.
{:refdef: style="text-align: center;"}
![pyinteraph scheme](../img/tut10/pyinteraph_cluster_size.png)
_(Image adapted from: Tiberti, M., et al (2014). Jour. chem. inf. mod., 54(5), 1537-1551.) Largest cluster size as a function of the persistence cutoff. The jump is the 'critical persistence' at which it makes sense to place the cut-off._
{:refdef}  

### Assumptions
Pyinteraph works on the following assumptions.
1. No PBC (if present, they must be removed first).
2. No large transitions (e.g. no rare events). The trajectory is assumed to be sampling a relatively local free energy basin.

## Installing pyinteraph
Pyinteraph is currently distributed as a python 2.7 package, although the
authors are working on a python3 version.  This raises a few problems, since
python 2.7 will be phased out shortly and python3 is not backward compatible.
As this is a frequent issue with scientific packages, we will look into two
different ways of working with pyinteraph: 1) installing it in a proper local
environment using conda, and 2) reimplementing just the portions of it we need. 
{:refdef: style="text-align: center;"}
![what to expect](../img/tut10/phdcomics-bugs.png)  
_The typical installation procedure for legacy software._
{:refdef}  


### Install locally
We will install this on the cluster since there you already have some of the requirements in the moduli. 
The same procedure should work on your laptop too with minimal modification (i.e. you will have
to install hdf5 and netcdf by yourselves).

First of all, download the source got from its [github repo](https://github.com/ELELAB/pyinteraph).
In order for the distributed pyinteraph to work, you need to make it believe it
nothing has changed around it since the last published version. Check the dates and modifications 
on the commits history of the repo.

When you download the software, you will notice in the `INSTALL` file a list of
dependencies (look at the instructions for Linux). Sometimes you do not have
the proper version number of a module or library, so you will have to take a
guess at the right one. To do so, go check [pypi](www.pypi.org), search the
package you are interested in, and look for its release history. You will have
to take note of the last version of the package prior to the distribution date
of pyinteraph.

To save you some work, this is the correct list of software you will need, set up
to create a python environment simulating late 2013.

##### Non-python libraries needed
In the cluster, you can get those from moduli. On your laptop, check the
apt-get instructions from the `INSTALL` file (or equivalent for MacOs).
* HDF5
* Netcdf4
* Gcc  
These are available as modules on the cluster. You can load them with:
```bash
module load netcdf-4.7.0--gcc-9.1.0
module load hdf5-1.8.18
```

#### Python & python packages
As written above, you will need `python2.7` plus 
* scipy
* matplotlib
* networkx
* cython
* biopython
* netCDF4
* MDAnalysis
{:refdef: style="text-align: center;"}
![pyinteraph scheme](../img/tut10/python2.7.jpg)  
_Pyinteraph is unfortunately still implemented in python2.7 (a new version is going to come out soon)._
{:refdef}  

To install the correct version of them (2013..) we will have to create another
environment with conda, and then install things there with pip, the python
package manager.

To create a conda environment named `pyint` and based on python 2.7, run:
```bash
Conda create -n pyint python=2.7
```
Or create a `.yml` file as follows, 
```
name: pyint
channels: 
    - conda-forge
dependencies:
    - python=2.7
    - jupyter
    - pip
```

And run it with 
```bash
conda env create --file myfile.yml # substitute the correct name of course
```

The second step is to install the missing packages. To do so, first you need to
activate your environment: 
```bash
conda activate pyint
``` 
Once you are in, you have to create two requirements files for `pip`. 
The first will have numpy, as it is a requirement for everything else. 
```
numpy==1.7.0
GridDataFormats==0.2.2
```
Then you have to create a file  `requirement-1.txt`, containing:
```
scipy==0.12.0
matplotlib==1.3.0
networkx==1.8
cython==0.19
biopython==1.61
netCDF4==1.0.8
MDAnalysis==0.7.6
```
And install everything with `pip install -r requirements-1.txt`. 

<p class="prompt prompt-question">What errors do you get? What do they mean?</p>

You have to create the requested environment variable to tell the compiler where
to find the headers (the .h files) it needs. With `module` you can find out
where the header files are stored using 
```bash
module show <module-name> #again, substitute the correct name
```
Once you found the directory, you have to `export` the variables and re-run `pip`.

OK. Now, we have all the correct modules installed! We can thus install the
package.  You have to run `setup.py` telling it to install things locally. 
```bash
python setup.py install —user
#install in home/name.surname/.local
```

We can now try to run the examples in the `examples` folder. Of course, we will have
to modify the `tutorial.sh` file. In two ways. One, to let it know where to
find pyinteraph and libinteract, as per instructions of the developers; two to
make it  so that bash knows where to look for the python environment you
created. 
```bash 
export PYTHONPATH=$PYTHONPATH:$HOME/.local/lib/python2.7
export PYINTERAPH=$HOME/.local/pyinteraph
export PATH=$PATH:$PYINTERAPH
# making conda visible 
source /home/luca.tubiana/usr/src/miniconda3/etc/profile.d/conda.sh
# activating the environment
conda activate pyint
```
<p class="prompt prompt-question"> why do you need to make the script aware of conda? </p>

Ok, we corrected this issue as well. Let’s run the script (again). 

<p class="prompt prompt-question">  Question: why does it crash now? </p>
Suggestion: add **which python** to tutorial.sh before the first call. Then
open pyinteraph with vim and check the very first line. 
```bash
#!/usr/bin/python  #never do this!
```

Check all the files called by `tutorial.sh`. Open them with a text editor and
change `#!/usr/bin/python`  with `#!/usr/bin/env python`. Install again the
package. 

The first line of a script, when it starts with a `#!` is called a "shebang" and
tells the system what interpreter to use. The difference between the two lines is 
that the original one tells the system to use a specific python installation, the 
system-wide one; the second line specifies instead to use whatever python has been
activated in the current environment, i.e. conda in your case. 

<p class="prompt prompt-info"> Extra points: upload everything to your gitlab repo! </p>

#### Rewrite the portions you need
Well, just download this [jupyter notebook](../materials/tutorial10/pyfferaph.ipynb).
Those are the [example data](../materials/tutorial10/example_traj_pyint.tar.gz)

Warning: The notebook was created only for recoverin. It works for that, but it
is not general and might punish you for every mistake. Let’s run it on the same
files from the tutorial. 

## Benchmarking

NAMD is not the only Molecular Dynamics software available: _Gromacs_ , _lammps_ , _Amber_, _Espresso_ etc.

The [HecBioSim project](http://www.hecbiosim.ac.uk/) provides an impressive
list of [Benchmarks](http://www.hecbiosim.ac.uk/archer-benchmarks) for five
proteins with different sizes simulated with different softwares and different
amounts of computational resources: 

#### 3NIR Crambin, a system with 20K atoms
{%include tut10_bench_fig1.html %}

#### 1WDN Glutamine-Binding Protein - 61K atoms
{%include tut10_bench_fig2.html %}

#### hEGFR Dimer of 1IVO and 1NQL  - 456K atoms
{%include tut10_bench_fig3.html %}

#### A Pair of hEGFR Dimers of 1IVO and 1NQL - 1.4M atoms
{%include tut10_bench_fig4.html %}

#### A Pair of hEGFR tetramers of 1IVO and 1NQL - 3M atoms
{%include tut10_bench_fig5.html %}

## ENM-based web servers
Elastic Network Models are a widely used tool to get useful insights about
local fluctuations of a structure around a **single** equilibrium conformation
without doing a MD simulation. The Hamiltonian of the Elastic Network Model is 
another direct application of graph theory, as it is basically a weighted 
adjacency matrix. As a consequence, it is symmetrical and its spectrum is what
interests us.

ENM Hamiltonian:

$$
V^{AT}_{ENM} ( r_{i} )= \frac{1}{2} K \sum_{i < j} C_{ij} (r_{ij} - r_{ij}^0)^2
$$

More formally:

- symmetric interaction matrix **M**

- 3N - 6 non-zero eigenvalues (if you remove rotations and translations)

- the principal modes of fluctuation correspond to the eigenvectors of **M** associated to the lowest non-zero eigenvalues

### Quasi-rigid domain decomposition

Observed robustness of the modes of structural fluctuations: large-scale fluctuations are often not affected by details in the chemical composition. It is natural to start from this experimental data to try to describe protein intenal dynamics in terms of relative motion of rigid units.

<IMG class="displayed" src="../../img/tut10/qrdd.png" alt="">

Residues undergoing concerted movements usually have positive correlation. Domains can be identified as clusters of positively-correlated residues.

<IMG class="displayed" src="../../img/tut10/img_concerted_movement.png" alt="">

This particular motion is not recognised as rigid.

Useful webservers:
1. [PiSqrd](http://pisqrd.escience-lab.org/) decomposes a protein of interest in quasi-rigid domains based on the minimisation of the intra-block fluctuations;
2. [HingeProt](http://www.prc.boun.edu.tr/appserv/prc/hingeprot/) finds the hinge region of the protein of interest;

Basically these two web servers do the same stuff, try them:
- with a _paradigmatic_ protein, like [1E5G](https://www.rcsb.org/structure/1E5G). Play with the parameters of ENMs;
- _4ake_, the _fruit fly_ of Computational biophysics;
- with recoverine;


<!-- <p class="prompt prompt-info">Until a few years ago, typical resolutions for EM maps of biomolecules were 15-30 Å, and high-resolution crystal structures were often available only for domains of a biomolecular complex (Frank, 2002; Rossmann, 2000). This led to the development of so-called _rigid-body docking techniques_, that fit atomic structures into density maps keeping the high-resolution structure rigid, usually by performing an exhaustive search over all rotational and translational degrees of freedom in real or reciprocal space, guided by some choice of similarity measure. </p>
<p></p>-->

# Notes
