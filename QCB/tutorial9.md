---
layout: page
authors: [Marco Giulini, Luca Tubiana]
mathjax: true
---

## Finalizing the kcsa simulation & setting up recoverin simulation
This lecture is divided in two parts. In the first we will complete
the tutorial for the simulation of the Potassium Ion Channel. In the
second part you will have to apply what you learned to our system of
interest: recoverin attached to a membrane.

## Potassium channel simulation

If you tried to run the constrained simulation of the potassium channel complex probably you've encountered an error similar to the following:

<p class="prompt prompt-shell">ERROR: Constraint failure in RATTLE algorithm for atom ATOM_NUMBER! \\
ERROR: Constraint failure; simulation has become unstable.\\
ERROR: Exiting prematurely; see error messages above.</p>

<p class="prompt prompt-question">Let's make sense
of these errors.</p>

# Analysing the .fix file

Open the pdb kcsa_popcwi.fix in Vmd.

Select Beta as Coloring Method and spot the atom that was responsible for the crash of the simulation.

<p class="prompt prompt-question">Do you notice anything weird?</p>

Some atoms of the lipidic heads are free to move, while many others are not. Why? Because we provided you with the atom IDs of _Charmm27 ff_ , while we are using _Charmm36 ff_ topologies.

Let's rebuild the fix file using the proper atom names.

- Open the _top_all36_lipid.rtf_ topology file and look for the POPC entry
- Have a look at the structure of the lipid
- Select the atom names that are part of the lipidic head (from the _PO4_ group onwards)
- create a _create_fix_36.tcl_ file able to generate a new _.fix_ file 

# Correcting the force fields

What is NBFIX? Molecular Dynamics simulations are spanning increasingly longer time scales, so the parameters that have been estimated in the past using short simulations are corrected over time.

<p class="prompt prompt-attention">Do not use _par_all27_prot_lipidNBFIX.prm_ file: it contains the NBFIXes and and some interaction parameters proper to _Charmm27 ff_ (wrong) \\
\\
Do not use _par_all27_prot_lipid_na.inp_ : it creates conflicts with _par_all36_prot.prm_
</p>

- Charmm36 NBFIX (feel free to use it or not)

It is possible to use the latest corrections to the Charmm36 non-bonded interactions developed by _The Aksimentiev Group_, [toppar_water_ions_cufix](https://www.dropbox.com/s/3v1ng0nwm74qciv/toppar_water_ions_cufix.str?dl=0). As usual, the file is not directly NAMD-compatible, it is necessary to comment out a few lines.

Now we are ready to run the constrained simulation properly.

# Second step
We will restart the simulation and let everything move,
but for the protein there will be harmonically restraints.
The aim is to let the membrane close the gap with the protein.

We will use the `tcl` script `keep_water_out.tcl` to expel the water molecules that flow until the system is compact enough to prevent it.
<IMG class="displayed" src="../../img/tut7/keep_wat.png" alt="">
We can define exception for water molecules nearby the protein. <!--(the `WF`, `WCA/B/C/D`). -->

# Third step
We release also the protein to perform the equilibration.

# Production
Finally, we can launch the actual simulation, keeping now constant
the area of the system.

<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>


<!--
## A new episode of...
<iframe class="center" frameborder="no" border="0" src="https://editor.p5js.org/Gianfree/embed/Sk4_1sphm" width="660px" height="260" ></iframe>

<p></p>
<p></p>
### MDFF
MDFF stands for _Molecular Dynamics Flexible Fitting_ and it is a protocol
to fit an atomistic structure into a _density map_ (see the [original paper](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2430731/) ).

The need for this kind of procedure arises from the increase in the cryo-em structures, for which there is a database similar to the Protein Data Bank,
the [Cryo-EM database](http://www.emdatabank.org/).

To achieve the wanted fitting several techniques could be employed; the _rigid-body_
docking; the _flexible fitting approach_, where the macromolecule is divided into rigid
pieces that are rigid-body docked and optimized with some refinement; elastic network models.

In this tutorial we will take advantage of the MD power to improve the _flexible fitting_
that can be obtained by other software (see the [reference article](https://www.sciencedirect.com/science/article/pii/S1046202309000887?via%3Dihub) on MDFF).



The implementation of MDFF requires a modified potential for our simulation:
<p align="center">
$$
U = U_{MD}  +  U_{CRYO-EM} + U_{SecStruct}
$$
</p>
where $$U_{MD}$$ is the usual ff, $$U_{SecStruct}$$ preserves the secondary structure
and avoid the introduction of wrong chirality, and/or cispeptide bonds.
The key part is related to the $$U_{CRYO-EM}$$:

$$
U_{CRYO-EM}(\{\vec{r}_i\}) = \sum_j w_j V_{CRYO-EM}(\vec{r}_j)
$$

with $$w_j$$ is a weight for each atom, and

$$
V_{CRYO-EM} =
\begin{cases}
  \xi \big[1 - \frac{\Phi(\vec{r}) - \Phi_{thr}}{\Phi_{max} - \Phi_{thr}}\big] &\quad\text{if } \Phi(\vec{r})\ge \Phi_{thr} \\
  \xi &\quad\text{if } \Phi(\vec{r})\lt \Phi_{thr} \\
\end{cases}
$$

with $$\xi$$ a scaling factor, and $$\Phi(\vec{r})$$ is the data of the CRYO-EM in a given voxel.
For $$\Phi_{max}$$ and $$\Phi_{thr}$$, look at the big screen.

For this tutorial you can download the necessary [files](https://www.ks.uiuc.edu/Training/Tutorials/science/mdff/mdff-tutorial-files.tar.gz) and the [instructions](https://www.ks.uiuc.edu/Training/Tutorials/science/mdff/tutorial_mdff.pdf) for the TCBG group in Urbana-Champaign.

-->

## Recoverin attached to the membrane
We will start from a configuration produced by Borsatto et al. in their
[work on wild type recoverin](https://www.mdpi.com/1422-0067/20/20/5009/pdf).
Please download the configuration at this 
[link](../materials/tutorial9/rec_r_membrane.pdb).  When you open it with 
VMD you will see that it contains the protein complexed with a membrane patch 
and the counter-ions.

From the given file, we need to reach a configuration from our system of interest,
i.e. a mutated recoverin complexed with the membrane including solvating water and
counterions, as in the image below.
{:refdef: style="text-align: center;"}
![the system to build](../../img/tut9/ionized_pretty.png)  
_The system we want to build and simulate. Water is represented as a
transparent surface. The top half of the membrane is represented as a
transparent surface through which it is possible to see the inserted myristoyl._
{:refdef}  

In order to do this, you will need to put together several steps from the previous 
tutorials.
1. Mutate the protein attached to the membrane, as in [tutorial6](./tutorial6).
2. Solvate the system 
3. Ionize the system, taking care of having a neutral system at the end.
4. Minimize (at least 1k steps).
5. Equilibrate the water and counterions (NVT 250 ps).
6. Equilibrate the membrane (NVT 250 ps).
7. Equilibrate the system (NVT 250 ps) -> Check that temperature converges.
8. Equilibrate the pressure (NPT 500 ps) -> Check that volume converges.
9. Production run, NPT (at home).

In the following we will give you only some hints, as you should already know how to 
perform all of those steps.

### 1. Mutating the protein
The way to do this is to use psfgen, as we did in [tutorial6](./tutorial6). Of course now you
have one more segment, the membrane..
<p class="prompt prompt-attention"> **Note** when generating the membrane patch you **must avoid** using 'auto none'</p>

### 2. Solvating the system
We will content ourselves with VMD solvation scheme, in this case it is enough. We also know the
dimension of the box from the original simulation of the WT, which we can use in the solvate procedure.
For example something like:
```tcl
# set psf ...
# set pdb ...
set box_size {93.116638 93.116638 150.0}
set box_min [vecscale -0.5 $box_size]
set box_max [vecscale 0.5 $box_size]

# Note that the two commands below solve the issue of passing arguments to solvate..
set solv "solvate $psf $pdb -o $tmp -minmax \{\{$box_min\} \{$box_max\}\}"
{*}solv 
``` 
You should first pose yourselves a few questions...
<p class="prompt prompt-question">Is the system centered?</p>
<p class="prompt prompt-question">What is the best way to avoid having water in between the membrane? </p>

#### 2.1 Avoiding water in the membrane
You have two possible ways
1. Use the same scheme as in [tutorial8](./tutorial8): solvate, then measure generously the membrane size,
and finally remove all the water in it using `psfgen`. 
2. Realize that water is put within the membrane only near the pbc cell walls, since the replica of the lipds are outside.
<p class="prompt prompt-question"> Can you think of any creative (but still sensible) ways to get rid of that water? </p>

Remember also that after solvate you can write the `pdb` and `psf` of just a selection of your system, e.g.
```tcl
set water [atomselect top "water and same residue as(z<$min_z or z>$max_z)"]
$water writepdb water.pdb
$water writepsf water.psf
```
<p class="prompt prompt-attention"> **'same residue as'** is essential to select also the bond information in the psf! </p>
<p class="prompt prompt-question"> What are sensible choices for 'z_min' and 'z_max'? How do you measure them? </p>

And that you can collate psf files later on:
```tcl
mol delete all
package require psfgen
resetpsf
readpsf $psf
coordpdb $pdbout
readpsf water.psf
coordpdb water.pdb
writepdb ${out}.pdb
writepsf ${out}.psf
```
#### 3. Ionization
The ions are the usual ones: 1 Mg2+ and 150 mMol KCl. Use the VMD interface.  
**Note** VMD gives you several option, one of which is "neutralize and add..." 
<p class="prompt prompt-question">Is is the same if you first add the Mg ion and then 
add the KCl or if you swap the operations?</p>  
**Spoiler:**  it is not.

##### 3.1 Ionizing from Tk console
It is possible to use the Tk console to ionize your system. To do so you have the command  `autoionize`.
Running it without argument will produce the inline help.

**Example**
```tcl
autoionize -psf ${in}.psf -pdb ${in}.pdb -o $tmp -nions {{MG 1}}
autoionize -psf ${tmp}.psf -pdb ${tmp}.pdb -o $out -sc 0.15 -cation POT
```

#### 4. Setting the simulation and minimization
You will need the following parameter files:
```tcl
parameters  ${toppar_path}/par_all36_prot.prm;
parameters  ${toppar_path}/par_all36_lipid.prm;
parameters  ${toppar_path}/par_all36_carb.prm;
parameters  ${toppar_path}/par_all36_cgenff.prm;
parameters  ${toppar_path}/par_all36_na.prm;
parameters  ${toppar_path}/stream/lipid/toppar_all36_lipid_prot.str
parameters  ${toppar_path}/stream/lipid/toppar_all36_lipid_sphingo.str
parameters  ${toppar_path}/stream/lipid/toppar_all36_lipid_miscellaneous.str
parameters  ${toppar_path}/toppar_water_ions_namd.str;
```

#### 5. Equilibrating the water
You need to select the water and the ions (except CAL!) and set these as free, while everything else
you keep fixed. To do so, use the Beta factor as usual.

**Example**
```tcl
set seltext "protein or name CAL or (resname DGPC DGPE DGPS)"

set a [atomselect top "all"]
set t [atomselect top $seltext]

$a set beta 1
$t set beta 0

$a writepdb water_free.fix
```

You need then to specify the constraints in the namd conf file:

```tcl
if {0} {  
  # use this to fix the lipid heds
  fixedAtoms          on
  fixedAtomsFile      ${name}.superfix
  fixedAtomsCol       B
  fixedAtomsForces    on
} else {
  constraints        on
  consexp             2
  consref             water_free.fix
  conskfile           water_free.fix
  conskcol            B
}
```
Please check the [namd user guide](https://www.ks.uiuc.edu/Research/namd/2.12/ug.pdf) to gain confidence about the above commands!

#### 6. Equilibrating the lipids tails.
In order to do this you need to fix everything else in the system. This is done as usual
by setting `beta` to `1` for the fixed atom in a pdb file which is then passed to namd.
How do we identify only the lipid tails for three different lipid types with atomselect?
<p class="prompt prompt-question"> How do we identify only the lipid tails for three different lipid types with atomselect?  </p>  

Check out  the file `toppar_all36_lipid_miscellaneous.str` and look for the three 
resid types `DGPS DGPE DGPC`. **Take a look at the comments!** They report the 
chemical structure of the lipids.
```
RESI DGPS        -1.0  ! di-gadoleic-phosphatidylserine (diC20:1)
GROUP                  !
ATOM N    NH3L   -0.30 !                HN2
ATOM HN1  HCL     0.33 !                 |
ATOM HN2  HCL     0.33 !                 |
ATOM HN3  HCL     0.33 !                 |
ATOM C12  CTL1    0.21 !       (+) HN1---N---HN3
ATOM H12A HBL     0.10 !                 |
GROUP                  !                 |     O13A (-)
ATOM C13  CCL     0.34 !                 |     ||
ATOM O13A OCL    -0.67 !          H12A--C12----C13---O13B
ATOM O13B OCL    -0.67 !                 |
GROUP                  !                 |     alpha5
ATOM C11  CTL2   -0.08 !                 |
ATOM H11A HAL2    0.09 !          H11A--C11---H11B
ATOM H11B HAL2    0.09 !                 |     alpha4
ATOM P    PL      1.50 !        (-) O13  O12
ATOM O13  O2L    -0.78 !              \ /      alpha3
ATOM O14  O2L    -0.78 !               P (+)
ATOM O12  OSLP   -0.57 !              / \      alpha2
ATOM O11  OSLP   -0.57 !        (-) O14  O11
ATOM C1   CTL2   -0.08 !                 |     alpha1
ATOM HA   HAL2    0.09 !            HA---C1---HB
ATOM HB   HAL2    0.09 !                 |     theta1
GROUP                  !                 |
ATOM C2   CTL1    0.17 !            HS---C2--------------
ATOM HS   HAL1    0.09 !                 | beta1        |
ATOM O21  OSL    -0.49 !            O22  O21          theta3
ATOM C21  CL      0.90 !             \\ /  beta2        |
ATOM O22  OBL    -0.63 !               C21              |
ATOM C22  CTL2   -0.22 !               |   beta3        |
ATOM H2R  HAL2    0.09 !        H2R---C22---H2S         |
ATOM H2S  HAL2    0.09 !               |                |
GROUP                  !               |    beta4       |
ATOM C3   CTL2    0.08 !               |                |
ATOM HX   HAL2    0.09 !               |           HX---C3---HY
ATOM HY   HAL2    0.09 !               |                |   gamma1
ATOM O31  OSL    -0.49 !               |           O32  O31
ATOM C31  CL      0.90 !               |            \\ /    gamma2
ATOM O32  OBL    -0.63 !               |              C31
ATOM C32  CTL2   -0.22 !               |              |     gamma3
ATOM H2X  HAL2    0.09 !               |        H2X---C32---H2Y
ATOM H2Y  HAL2    0.09 !               |              |
GROUP                  !               |              |      gamma4
ATOM C23  CTL2   -0.18 !               |              |
ATOM H3R  HAL2    0.09 !        H3R ---C23---H3S      |
ATOM H3S  HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C24  CTL2   -0.18 !               |              |
ATOM H4R  HAL2    0.09 !        H4R ---C24---H4S      |
ATOM H4S  HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C25  CTL2   -0.18 !               |              |
ATOM H5R  HAL2    0.09 !        H5R ---C25---H5S      |
ATOM H5S  HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C26  CTL2   -0.18 !               |              |
ATOM H6R  HAL2    0.09 !        H6R ---C26---H6S      |
ATOM H6S  HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C27  CTL2   -0.18 !               |              |
ATOM H7R  HAL2    0.09 !        H7R ---C27---H7S      |
ATOM H7S  HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C28  CTL2   -0.18 !               |              |
ATOM H8R  HAL2    0.09 !        H8R ---C28---H8S      |
ATOM H8S  HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C29  CTL2   -0.18 !               |              |
ATOM H9R  HAL2    0.09 !        H9R ---C29---H9S      |
ATOM H9S  HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C210 CTL2   -0.18 !               |              |
ATOM H10R HAL2    0.09 !        H10R---C210--H10S     |
ATOM H10S HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C211 CEL1   -0.15 !               |              |
ATOM H11R HEL1    0.15 !        H11R---C211           |
GROUP                  !               ||    (CIS)    |
ATOM C212 CEL1   -0.15 !               ||             |
ATOM H12R HEL1    0.15 !        H12R---C212           |
GROUP                  !               |              |
ATOM C213 CTL2   -0.18 !               |              |
ATOM H13R HAL2    0.09 !        H13R---C213--H13S     |
ATOM H13S HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C214 CTL2   -0.18 !               |              |
ATOM H14R HAL2    0.09 !        H14R---C214--H14S     |
ATOM H14S HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C215 CTL2   -0.18 !               |              |
ATOM H15R HAL2    0.09 !        H15R---C215--H15S     |
ATOM H15S HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C216 CTL2   -0.18 !               |              |
ATOM H16R HAL2    0.09 !    H16R---C216--H16S         |
ATOM H16S HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C217 CTL2   -0.18 !               |              |
ATOM H17R HAL2    0.09 !        H17R---C217--H17S     |
ATOM H17S HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C218 CTL2   -0.18 !               |              |
ATOM H18R HAL2    0.09 !        H18R---C218--H18S     |
ATOM H18S HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C219 CTL2   -0.18 !               |              |
ATOM H19R HAL2    0.09 !        H19R---C219--H19S     |
ATOM H19S HAL2    0.09 !               |              |
GROUP                  !               |              |
ATOM C220 CTL3   -0.27 !               |              |
ATOM H20R HAL3    0.09 !        H20R---C220--H20S     |
ATOM H20S HAL3    0.09 !               |              |
ATOM H20T HAL3    0.09 !              H20T            |
GROUP                  !                              |
ATOM C33  CTL2   -0.18 !                              |
ATOM H3X  HAL2    0.09 !                       H3X ---C33---H3Y
ATOM H3Y  HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C34  CTL2   -0.18 !                              |
ATOM H4X  HAL2    0.09 !                       H4X ---C34---H4Y
ATOM H4Y  HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C35  CTL2   -0.18 !                              |
ATOM H5X  HAL2    0.09 !                       H5X ---C35---H5Y
ATOM H5Y  HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C36  CTL2   -0.18 !                              |
ATOM H6X  HAL2    0.09 !                       H6X ---C36---H6Y
ATOM H6Y  HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C37  CTL2   -0.18 !                              |
ATOM H7X  HAL2    0.09 !                       H7X ---C37---H7Y
ATOM H7Y  HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C38  CTL2   -0.18 !                              |
ATOM H8X  HAL2    0.09 !                       H8X ---C38---H8Y
ATOM H8Y  HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C39  CTL2   -0.18 !                              |
ATOM H9X  HAL2    0.09 !                       H9X ---C39---H9Y
ATOM H9Y  HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C310 CTL2   -0.18 !                              |
ATOM H10X HAL2    0.09 !                       H10X---C310--H10Y
ATOM H10Y HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C311 CEL1   -0.15 !                              |
ATOM H11X HEL1    0.15 !                       H11X---C311
GROUP                  !                              ||    (CIS)
ATOM C312 CEL1   -0.15 !                              ||
ATOM H12X HEL1    0.15 !                       H12X---C312
GROUP                  !                              |
ATOM C313 CTL2   -0.18 !                              |
ATOM H13X HAL2    0.09 !                       H13X---C313--H13Y
ATOM H13Y HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C314 CTL2   -0.18 !                              |
ATOM H14X HAL2    0.09 !                       H14X---C314--H14Y
ATOM H14Y HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C315 CTL2   -0.18 !                              |
ATOM H15X HAL2    0.09 !                       H15X---C315--H15Y
ATOM H15Y HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C316 CTL2   -0.18 !                              |
ATOM H16X HAL2    0.09 !                       H16X---C316--H16Y
ATOM H16Y HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C317 CTL2   -0.18 !                              |
ATOM H17X HAL2    0.09 !                       H17X---C317--H17Y
ATOM H17Y HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C318 CTL2   -0.18 !                              |
ATOM H18X HAL2    0.09 !                       H18X---C318--H18Y
ATOM H18Y HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C319 CTL2   -0.18 !                              |
ATOM H19X HAL2    0.09 !                       H19X---C319--H19Y
ATOM H19Y HAL2    0.09 !                              |
GROUP                  !                              |
ATOM C320 CTL3   -0.27 !                              |
ATOM H20X HAL3    0.09 !                       H20X---C320--H20Y
ATOM H20Y HAL3    0.09 !                              |
ATOM H20Z HAL3    0.09 !                              H20Z
```

<p class="prompt prompt-attention"> **Note that the numeration of the atoms is regular.** </p>
The numeration of the atoms follows in fact the main branches of the polymeric chain.
```
     *
     .
     .
     .
    [11]
     |
    [1]
     |
    ---
   /   \
 [2]   [3]
  |     |
 [21]  [31]
  .     .
  .     .
  .     .
  *     *
```
in the tails are
If you look more carefully, you will notice that in the two tails:
1. All the `C` atoms are numbered as "C2*" or "C3*"; 
2. The same goes for the oxygens: "O2*", "O3*".
3. All the `H` names finish in one of these lettes: "R S X Y T Z"

<p class="prompt prompt-question"> **How do we tell VMD to select according to those rules?** </p>
<p class="prompt prompt-attention"> Using **[regular expressions](https://www.ks.uiuc.edu/Research/vmd/vmd-1.3/ug/node137.html#SECTION001050000000000000000)** </p>

| Symbol | Example | Definition |
| --- | --- | --- |
| `.`   |  `.`, `A.C`   | Match any character |
| `[]`   | `[ABCabc]` , `[A-Ca-c]` | match any char in the list |
| `[~]`  | `[~XYZ]` , `[^x-z]` | match all except the chars in the list |
| `^`    | `^C` , `^A.*` | next token must be the first part of string |
| `$`    | `[CO]G$`      | prev token must be the last part of string |
| `*`    | `C*` , `[ab]*` | match 0 or more copies of prev char or regex token |
| `+`    | `C+` , `[ab]+` | match 1 or more copies of the prev token |
| `\|`   | `C\|O`        | match either the 1st token or the 2nd |
| `\(\)` | `\(CA\)+`     | combines multiple tokens into one |

**Example: selecting all tails.**
```tcl
set tail [atomselect top {resname "DGP." and (name "[CO]2.*" or name "[CO]3.*" or name "H.*[RSXYTZ]")}]
```
<p class="prompt prompt-attention"> note that you want to keep also the myristoyl free, so the above sel is not enough.. </p>
