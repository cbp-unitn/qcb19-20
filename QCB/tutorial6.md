---
layout: page
author: Luca Tubiana
mathjax: true
---
<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>


## Simulating & mutating recoverin + Using [git](https://git-scm.com/)

In this lecture we will see:
1. Overview of the second half of the course!
2. We will finish the NVT
3. What mutations of recoverin are interested in, and why.
4. Restarting a previous simulation.
5. How to use git to collaborate on the project. [^1]

[^1]: This will be a short introduction. For more material see
 the [git-pro book](https://git-scm.com/book).

## Roadmap for the second half of the course
This is the plan for the second half of the course.
1. **28.10** Mutations of recoverin; relaunching sims; git overview.
2. **11.11** Analysis of the first batch of results on mutated recoverin. Membrane proteins I: how to setup a sim.
3. **18.11** Membrane proteins II: potassium channel.
4. **25.11** Membrane proteins III: adapting the techniques for the recoverin study.
5. **2.12**  Advanced analysis tools (webservers & python packages).
6. **9.12**  Discussion of your (partial) reports.
7. **16.12** Enhanced sampling techniques!

## Recoverin in Explicit Solvent (NVT)

If you tried to simulate the protein, probably you have encountered the following error message:

<p class="prompt prompt-attention">Charm++ fatal error: FATAL ERROR: UNABLE TO FIND BOND PARAMETERS FOR C CTL2 (ATOMS 8 10)</p>

It means that inside the parameter files we loaded there was not a definition of the bond between the first two carbon atoms of GLYM. This bond is defined inside _toppar_all36_lipid_sphingo.str_ a file that you can find inside the stream/lipid folder of your toppar directory. As in the case of _toppar_water_ions.str_ the file is not directly compatible with NAMD. You can do two different things

1. download the correct version from [here](https://drive.google.com/open?id=1gf7aMd8Oo8DuwHGE-hZ5QhfwFuvJBbwO)
2. comment the following lines
```tcl
	set nat ?NATC
	set app 
	if "@NAT" ne "?NATC" if @nat ne 0 set app append
```
	and substitute the line 
```tcl
	read rtf card @app
```
with 
```tcl	
	read rtf card append
```

The problem resides in the fact that the CHARMM force-field and NAMD have a conflict in the way in which they define the order in which parameter files have been loaded. This does not have any consequence on the force-field parameters.

So, let's set up a small simulation in explicit solvent. We have the solvated structure and the corresponding namd.conf file. We would like to run a small equilibration (500 picoseconds) in the NVT ensemble, and then a longer (but still reasonable) equilibration in the NPT ensemble.

### NVT

Open your conf file and go to the part in which we couple the system to the Langevin piston: this performs Constant Pressure Control, so we have to remove it.

*Golden rule*: do not comment out potentially useful commands, use an if clause:

```tcl
if {0} {
useGroupPressure      yes ;# needed for rigidBonds
useFlexibleCell       no
useConstantArea       no

langevinPiston        on
langevinPistonTarget  1.01325 ;#  in bar -> 1 atm
langevinPistonPeriod  100.0
langevinPistonDecay   50.0
langevinPistonTemp    $temperature
}
```

Now let's change the simulation commands in order to get 50 frames and a trajectory of 500 ps.

```tcl
restartfreq         50000    
dcdfreq             5000     
xstFreq             5000
outputEnergies      100
outputPressure      100

minimize            500
reinitvels          $temperature

run 250000 ;
```

what is _restartfreq_?

<p class="prompt prompt-question"> **Launch the simulation on HPC2** <br> * Use the PBS template in your folder.<br> * set the **walltime to 1 hour**. </p>

## Mutating recoverin 
As we said in [tutorial3](/pages/QCB/tutorial3), part of your exam will focus on 
simulating different mutations of recoverin:
1. **D74A**  
2. **D110A**
3. **M132A**
4. **I155A**
5. **A89G**

The mutations code works as follow: `[original residue][resid][mutated
residue]`. The letters are amino-acids codes in the 
[FASTA format](https://web.cas.org/help/BLAST/topics/codes.htm). You will 
notice that most mutations go into `A`, Alanine. Alanine is the smallest a.a.,
whose sidechain is made of a single `-CH3` group. The result of this
procedure, an 'Alanine scanning', is to **remove interactions** from the
protein, and study whether their absence change the properties of the
macromolecule.  In fact, the 5 selected residues were identified in the
[Borsatto et al](https://www.mdpi.com/1422-0067/20/20/5009) as being important for the interaction network
of recoverin. Let's see them.  
{:refdef: style="text-align: center;"}
![mutations](../../img/tut6/mutations.png)  
_The mutations you will study, with the nearby residues represented as licorice_
{:refdef}  

### Introducing the mutations in practice.
We will continue with the time-honored practice of 'learning by doing'.
<p class="prompt prompt-attention"> **Assignment**</p>
Use VMD to mutate the protein, then minimize the energy with NAMD and
finally solvate the mutated protein and add the ions.

**Extra points**: Produce a script for each step of the procedure (you 
are strongly encouraged to do this at home if not now).

<p class="prompt prompt-info"> **Hint:** log to console, save the console data to a file... </p>

<p class="prompt prompt-question"> **Ingredients** </p>
1. The pdb of recoverin (you have it in your folder).
2. The psf of recoverin (you should have it too..).
3. VMD & the psfgen script from [tutorial5](tutorial5)


{:refdef: style="text-align: center;"}
![VMD mutate interface](../../img/tut6/vmd-mutate-interface.png){:heigth="50%" width="50%"}  
_VMD has a tool, `Extension -> Modeling -> Mutate Residue`. Click on it to
obtain this window. If you then input the mutation you want, you will make VMD crash._
{:refdef}  

**The correct way** to perform point mutations in VMD is to modify the `psfgen` script we introduced 
[last time](tutorial5). psfgen supports a command `mutate`. From the [psfgen manual](https://www.ks.uiuc.edu/Research/vmd/plugins/psfgen/ug.pdf):

<p class="prompt prompt-info"> 
**mutate \<resid\> \<resname\>**
Purpose: Change the type of a single residue in the current segment.  
Arguments:  
\<resid\>:  Unique name for residue, 1--5 characters, usually numeric.  
\<resname\>: New residue type name from topology file.  
**Context: Within segment, after target residue has been created.**
</p>
**Note**: resname is a three letter code. You can find both the 1-letter and 3-letter codes
[here](https://www.dnastar.com/MegAlign_Help/index.html#!Documents/iupaccodesforaminoacids.htm).

<p class="prompt prompt-question"> What does **"within segment"** mean? </p>
<p align="center">
<br>
<br>
<br>
<br>
<br>
<br>
<br>
</p>

Example: to mutate  resid 1 to Alanine:
```tcl
segment P {
	pdb protein.pdb 
	mutate 1 ALA
	first none 
	last CNEU
}
```

<p class="prompt prompt-question"> **Steps** </p>
1. Produce the `.psf` and `.pdb` files for the mutated protein in vacuum.
2. Minimize with NAMD

<p class="prompt prompt-question"> **Steps:** There are two possible approaches:
1. mutate -> solvate & ionize -> minimize -> equilibrate  
2. mutate -> minimize using implicit solvent -> solvate & ionize -> minimize -> equilibrate.
It will be useful to try both and compare the results.
</p>


<p class="prompt prompt-question"> **Question:** why not mutating the solvated and ionized .psf directly? </p>

## Restarting a simulation! 

All-atom MD simulations are on average computationally expensive and
time-consuming, and there are many possible ways in which they may crash. In
this unlucky but not unlikely case, we don't want to restart all the
calculations from the timestep 0. In order to do so, we dump a restart file
with a certain frequency, that allows us to (guess what) restart the simulation
from that specific frame preserving all the parameters of the crashed
simulation, including positions and velocities.

- dumping restart files consumes computational resources. *Do not set a low value to _restartfreq_*
- restart files are overwritten by default, but you can set
```tcl
restartsave yes
```
and append the timestep to the name of the restart file.

How to start a new simulation from a restart file?

Pretty easy: just add add these lines below structure and coordinates (changing paths and file names according to your data). 

```tcl
bincoordinates     $PATH_TO_RESTART_FILES$/$RESTART_FILE_NAMES$.restart.coor
binvelocities 	   $PATH_TO_RESTART_FILES$/$RESTART_FILE_NAMES$.restart.vel
extendedSystem     $PATH_TO_RESTART_FILES$/$RESTART_FILE_NAMES$.restart.xsc
```
The _.xsc_ file contains the periodic cell parameters and some extended system
variables, that depend on the ensemble and the thermostat.

You also have to remove the temperature. It is already specified inside the velocity _.vel_ file.

```tcl
#temperature         $temperature
```
Try it with a restart file produced by your NVT simulation.
<p class="prompt prompt-question"> **Launch the simulation on HPC2** <br> * Use the PBS template in your folder.<br> * set the **walltime to 30 minutes**. </p>

### NPT (do it when NVT is done)

1. Load the NVT trajectory on VMD
2. Plot some observables (like V and T for instance)

In order to run the simulation it is sufficient just to replace the previous statement with a 1 inside the if clause and change the overall simulation time. But we need the initial configuration: from VMD you can go to _File->Save Coordinates_ and save the last frame of our NVT simulation.


<p class="prompt prompt-question">**Task:**<br> now we would like to run 5 ns of simulation saving configurations every 20 ps.<br>**Can you do it?**  </p>


## Using git
### What is git? Why should I care?
Git, like all serious version control systems, tries to solve two main problems.
1. **Keeping track of all changes** to files.
2. Let people **collaborate** on complex projects **effectively**.

A Version Control System (VCS) allows you to:
* revert selected files back to a previous state,
* revert the entire project back to a previous state,
* compare changes over time,
* work in two or more people on the same project,
* see who last modified something that might be causing a problem,
* ...

<p class="prompt prompt-attention">
Using a VCS also generally means that if you screw things up or lose files, you
can easily recover. </p>

#### Version control
A basic version of a VCS would be to simply have a local database in which you
store subsequent versions of the same file(s).
{:refdef: style="text-align: center;"}
![Versioning](../../img/tut6/local_versions.png){:heigth="50%" width="50%"}  
_The old idea: keep track of your changes_
{:refdef}  

In practice, this often become just the same file saved with the suffix..
`_v2`, `_v3`,..., `_final1`, `_final2`, `_final_final`...Entropy definitely
increases in a file system with time, as everywhere else. 

The idea is to use a  VCS like `git` to reduce the entropy as much as possible
with the minimum energy input, i.e. the minimum effort on your side. Plus, you 
want to cooperate with people! 

{:refdef: style="text-align: center;"}
![how it is done..](../../img/tut6/how_it_normally_ends.png){:width="80%"}  
_And how it is usually done..._
{:refdef}

A way to have something cooperative could be to put the whole DB (the
**"repository"** ) on a single server accessible by all contributors. This has
some problems though: i) if the server crashes, your can not work. ii) If the
server gets lost, your work gets lost as well. iii) lag times!

#### Distributed version control 
To solve the issues above, git implements what is known as **distributed version
control**. This means that the copy of the repository storing all the history
of changes is stored on the computer of each contributor, as well as on the
server.
{:refdef: style="text-align: center;"}
![how it is done..](../../img/tut6/distributed_versioning.png){:width="60%"}  
_And how it is usually done..._
{:refdef}
* Each contributor is on the same page.
* Even if the server is done for, you can retrieve the data.
* You can do a lot of work locally, then push things on the remote repository
  only in a second time.

#### How git saves your project 
Git creates a mini file-system, saving "snapshots" of your project directory
each time you "commit" your changes. Each snapshots stores **all the data in the
directory**. To save space, git substitutes unchanged files with links to their 
latest version.

{:refdef: style="text-align: center;"}
![how git works](../../img/tut6/git_versioning.png){:width="80%"}  
_Git version control. Unchanged files (links) are represented with dashed contours._
{:refdef}

**Strong points of git**
1. All operations are local -> fast!
2. It is possible to perform large changes without messing up everything.
3. It is **impossible** to change something on the repository without git knowing it!
4. Snapshots are additive: **no data is lost** (of course you can make it useless, but not lose it).

### Git three states
To allow git to do its job, you need to tell it what changes you want to push to the repository!
<p class="prompt prompt-attention"> You need to tell git what changes you want to save.  </p>

To give you flexibility in deciding what to save and what not to save, **git implements three
"areas"**, to which correspond four possible **file states**.

| area | file state | description |
| ---- | ---------- | ------------|
| _working directory_ | `untracked` | Files unknown to git. | 
| _working directory_ | `modified` | Files known to git that have been modified on your pc, not yet staged to enter a snapshot. | 
| _Staging area_ | `staged` | Files whose changes will enter the next snapshot to be committed |
| _Local repository (.git directory)_ | `committed` | Latest local snapshot of your project |

{:refdef: style="text-align: center;"}
![how git works](../../img/tut6/git-staging-area.svg){:width="80%"}  
_The staging area stores incremental changes before you commit them to a snapshot._
{:refdef}
Note the arrow **back** from the **repository**.  That
means that you canretrieve changes from your local repository to correct some
error, or to go back to a previous version of a file and experiment some
different change. 

The figure above only considers files that are already known to git. You can include unknown
files to the staging area with `git add`. A typical situation looks like this:
{:refdef: style="text-align: center;"}
![how git works](../../img/tut6/git_file_lifecycle.png){:width="80%"}  
_The lifecycle on a file on git. **commit** adds a file to the local repository. Therefore,
after that, there will be no difference between the copy on the snapshot and your local copy, which is then marked as `unmodified`._
{:refdef}

You can see the status of various files whenever you wish, by writing `git status`. Here is an 
example from the git-project we use to prepare your lectures.
{:refdef: style="text-align: center;"}
![how git works](../../img/tut6/git_status.png)  
_a practical example: a step during the preparation of this lecture_
{:refdef}

There is then the **remote repository** which is **shared with others**; to this you can push
only snapshots that are in your local repository.

### Using branches to  collaborate effectively
The nice things about version control, and in particular modern VCS is that they 
can manage co-editing. I.e. when two people do modify the same file, the VCS is 
usually able to merge the changes from both of them in a seamless way. That is,
until both tried to modify the same line in a file. That results in a **conflict**.
{:refdef: style="text-align: center;"}
![how git works](../../img/tut6/conflict.svg){:heigth="80%" width="80%"}  
_A and B both modified the same region in the file. Which one should be picked?_
{:refdef}

Once you encounter a conflict -git will tell you, trust me-  you will have to 
instruct what version to pick by yourself. On the incriminated file(s) you will 
find regions marked as follow:
```bash
<<<<<<< HEAD:file.txt
Hello world
=======
Goodbye
>>>>>>> 77976da35.....:file.txt
```
Where: `<<<<< HEAD` means **your** local copy and everything from `====` to
`>>>> 77976da...` means **their** copy, the one you pulled from the remote
repo.  You will have to choose which one to choose by removing the other one
AND the markers.

#### How to avoid conflicts in the first place?
By using **branches**.  

Git has the wonderful characteristic that it makes it extremely easy to create
a new snapshot of your data, called a branch, which is independent from the
others. Consider it as a second directory for your project, in which you can
play without modifying what was stored in the first one. With the plus that git
is still keeping track of all changes between your "branched directory" and the
"master directory".
{:refdef: style="text-align: center;"}
![how git works](../../img/tut6/branch_depiction.png){:heigth="80%" width="80%"}  
_adapted from [here](https://github.com/AlJohri/syr-djbootcamp18/blob/master/git/branching.md)_
{:refdef}

#### How should one structure branches? 
Branches are effective if you use them in a structured way. A successful git workflow
introduce by Vincent Driessen in 2010 and widely adopted. You can find it explained in 
detail [here](https://nvie.com/posts/a-successful-git-branching-model/).
{:refdef: style="text-align: center;"}
![how git works](../../img/tut6/git-model_nvie.png){:heigth="60%" width="60%"}  
_a successful git branching model](https://nvie.com/posts/a-successful-git-branching-model/)_
{:refdef}

There is another trick to it then, but we will see it later..

### Registering on gitlab + installing git on your machine.
We have set up a **virtual machine to host gitlab** at the **physics
department**. You can organize yourselves to use other hosts, but we strongly
recommend using this one instead. 

#### 1. Register on our internal gitlab host. 
Go to the [SBP gitlab host](http://jupyterlab.physics.unitn.it/gitlab) and register using your unitn email and name.surname as user account.
Note: this website is **not available outside the university network**. From home you need to use the [vpn-out](https://wiki.unitn.it/pub:conf-vpn-out).
{:refdef: style="text-align: center;"}
![how git works](../../img/tut6/gitlab_login.png)  
_Gitlab welcome page_
{:refdef}
#### 2.  I will add you to the group `cbp2019-2020`.
#### 3. Create a project on gitlab
We will see how to create a project from gitlab. **Create only ONE project per each group**. The projects should be called as follow:
   - G01-D74A
   - G02-D110A
   - G03-M132A
   - G04-I155A
   - G05-A89G

#### 4.  Link the project
Link the project you created to the project folder on hpc2 (and on your laptop:
you can create the folder there too!!). Normally you can follow the
instructions on gitlab, but in your case you already have the same files
written in the template folder, so we need to be sure to **avoid conflicts**.
There is a bit of an issue since we should have introduced git before the
template folder..We will follow these steps:
1. One person in your group will create the repo, from the **empty** template folder.
2. Both members of the group will copy what relevant in the newly created folder, pushing
to different branches.
3. We will try to merge your changes to a common branch, and see what happens..

#### 4.1 linking and initializing the repo
**First let git know who you are**  
```bash
git config --global user.name "name surname"
git config --global user.email "name.surname@studenti.unitn.it"
```
**Initialing the remote repo. ONE person only** (example for group1)   
```bash
# rerun cookiecutter, instructing it to keep your previous data (no need to insert it again, but choose a different parent folder..).
cookiecutter --replay git+http://jupyterlab.physics.unitn.it/gitlab/luca.tubiana/cbp-recoverin-project.git 
# create master branch
cd G01-recoverin-mutation
git init
git remote add origin git@192.168.131.119:cbp19-20/g01-d74a.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
The second member of the group will have to choose an empty folder and run
the following command to obtain the repository.
```bash
git clone git@192.168.131.119:cbp19-20/g01-d74a.git
```
#### 4.2 Adding your data
If you followed the protocol right, you should have the data from this morning simulations 
stored nicely in your subfolder `GXX-data/01-raw/01-explicit.` Copy it to the same path
inside your newly created git directory (`cp -r ...`).

After that, we want to add the files to the repository. Avoiding conflicts if possible.
We will try to do that by using **branches**. After you copied your data, 
go to the directory in which the data is stored and run the following.
```bash
git checkout -b your_name # of course substitute your_name appropriately.. :) This will create a branch named after you.
git add .
git status # what is git status showing?
git commit -m "write a proper message here!" # do it!
git push -u origin your_name # again, subsistute appropriately..
```
The above commands will switch git from the master branch to a branch named after you, where you can play
without the fear of losing something important. You can manage branches with the following commands:
```bash 
git branch  # show all local branches
git branch -a # show all branches, including remote ones
git checkout -b branch_name # create branch 'branch_name'
git checkout branch_name # switch to branch 'branch_name'
git branch -D branch_name # DELETE branch 'branch_name' locally
```
#### 4.3 Collaborating effectively
<p class="prompt prompt-info"> Now go to gitlab and update the project page, we will work on that. </p>

Let's take a look at our repository. On the left bar, click `repository`,
`branches`. You should see three branches: two personal ones and master. Now,
click on `new branch` to create a new one. Of course, just one of you should do
it. Call the new branch `develop` and select `create from master`.
{:refdef: style="text-align: center;"}
![mutations](../../img/tut6/gitlab_branches.png)  
_An example screen.._
{:refdef}  

Now that you have a develop branch, use the interface to merge one of your
branches to develop. Go to `merge` anche choose `develop` as a target instead
of `master`. Do all the procedure and **describe your merge request properly**.

Finally, we want to merge the second personal branch, let's say *pippo* if the
first was *ciccio* onto develop. *ciccio* contents are now already in develop.
Now, do you remember all our discussions about avoiding conflicts? This is the
extra trick. *Pippo* will have to do the following **before** submitting a
merge request to develop:
```bash
git fetch # update all branches locally, without pulling the changes
git checkout develop
git checkout pippo # again, use the correct name..
git merge develop # !!! this will merge develop INTO pippo
# solve any conflict that should arise, and commit again.
git status
git commit -m "merged develop into pippo"
git push
```
Only after doing this, *Pippo* can follow the same procedure as above to merge
into develop from the web interface. Why this? Because this way *pippo* who knows
better what changes he introduced can quickly solve any possible issues, before
asking for a merge request which might incur in larger conflicts (e.g. if something
changed on develop while he was working).

Remember the following golden rules:
* Create a branch for any change you want to implement.
* Do commit frequently.
* Merge from the starting branch (should be develop) back to your branch before pushing a merge request.
* Comment commits and merge requests exhaustively.
* Link branch names to tickets in gitlab...

###  keep track of things with gitlab.
Gitlab supports among other things the creation of issues. These are wonderful
to organize your work, particularly when you have to collaborate. Even better
if you start calling the branches by referencing the issue number..
{:refdef: style="text-align: center;"}
![issues](../../img/tut6/gitlab_tickets_board.png)  
_An example screen from our course preparation_
{:refdef}  

### Summary of git basic commands.

| command | effect | example |
| ---- | ---------- | ------------|
| git help     | Get help on git commands!                          | `git help [command]`       |
| git clone    | Clone a remote repository in the current dir.       | `git clone <git@>`         |
| git init     | Initialize the current directory as a repo      |   `git init`        |
| git add      | Add one or more files to the staging area      |   `git add file1 file2 file3..`        |
| git commit   | Add the files in the staging area to the local repository     |  `git commit [-m "commit message..."]`     |
| git checkout | Recover a file from the repo or switch branches    |   * `git checkout myfile #get file "myfile"`, <br> * `git checkout mybranch #get branch "mybranch"`,<br> * `git checkout -b newbranch #create new branch "newbranch"`      |
| git rm       |  remove files from the repository (same options as normal `rm`)      |   `git rm file` or `git rm -r dir`         |
| git mv       |  move files inside the repository (same as normal `mv`)      |   `git mv fileA fileB`        |
| git pull     |  Integrate changes from a remote repository into the local branch      |  `git pull`        |
| git push     |  Push local changes to a remote repository    |  `git push`        |

