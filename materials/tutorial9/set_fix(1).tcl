set seltext "protein or name CAL or (resname DGPC DGPE DGPS)"

set a [atomselect top "all"]
set t [atomselect top $seltext]

$a set beta 1
$t set beta 0

$a writepdb water_free.fix
