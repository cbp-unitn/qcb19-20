set pdb rec_r_ASP74ALA_membrane.pdb
set pdbout rec_r_ASP74ALA_membrane_center.pdb
set pdbpbcout rec_r_ASP74ALA_membrane_pbc.pdb
set psf rec_r_ASP74ALA_membrane.psf
set tmp rec_r_ASP74ALA_membrane_solv_TEMP
set out rec_r_ASP74ALA_membrane_solv

mol new $pdb

package require solvate
set box_size {92.116638 92.116638 150.455643}
set box_min [vecscale -0.5 $box_size]
set box_max [vecscale 0.5 $box_size]
set all [atomselect top "all"]
set cm [measure center $all]
$all moveby [vecinvert $cm]
$all writepdb $pdbout
pbc set $box_size
pbc wrap -centersel "all" -center com
$all writepdb $pdbpbcout

set solv "solvate $psf $pdbpbcout -o $tmp -minmax \{\{$box_min\} \{$box_max\}\}"
{*}$solv
set lip_h [atomselect top "resname DGPC DGPE DGPS and name C2"]
set lip_h_mM [measure minmax $lip_h]
set min_z [lindex $lip_h_mM 0 2]
set max_z [lindex $lip_h_mM 1 2]

set water [atomselect top "water and same residue as (z<$min_z or z>$max_z)"]
$water writepdb water.pdb
$water writepsf water.psf

mol delete all
package require psfgen
resetpsf
readpsf $psf
coordpdb $pdbout
readpsf water.psf
coordpdb water.pdb
writepdb ${out}.pdb
writepsf ${out}.psf

file delete ${tmp}.pdb
file delete ${tmp}.psf
file delete ${tmp}.log



