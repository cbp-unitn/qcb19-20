# Creating the psf for the rec_r

# -- parameters
set recoverin_path rec_r.pdb
set toppar_path "/home/tubiana/Work/Teaching/2019-2020/QCB-ComputationalBiophysics/cbp-unitn.gitlab.io/materials/common/toppar-charmm"

# -- input 
if {$argc != 3 } { 
	error "Usage: vmd -args resid -args mutation < make_psf_mutated.tcl"
}

set resid [lindex $argv 0]
set mutation [lindex $argv 1]

# -- run
# (1) load coordinates
mol new $recoverin_path
set res [atomselect top "resid $resid"]
set aa_old  [lindex [$res get resname] 0] 

# (2) write corrected and separated pdbs
set calcium [atomselect top "name CAL"]
$calcium writepdb calcium.pdb

set glym [atomselect top "resname GLM"]
$glym set resname "GLYM"

set protein [atomselect top "protein or resname GLYM"]
$protein writepdb protein.pdb

# (3) load topologies
package require psfgen
resetpsf
topology $toppar_path/top_all36_prot.rtf
topology $toppar_path/top_all36_lipid.rtf
topology $toppar_path/top_all36_na.rtf
topology $toppar_path/top_all36_carb.rtf
topology $toppar_path/top_all36_cgenff.rtf
topology $toppar_path/stream/lipid/toppar_all36_lipid_prot.str
topology $toppar_path/toppar_water_ions_namd.str
# see https://www.ks.uiuc.edu/Research/namd/mailing_list/namd-l.2013-2014/2214.html
# for the choice of toppar_water_ions_namd.str


# (5) set protein chain
segment P {
    pdb protein.pdb
    mutate $resid $mutation
    first none
    last CNEU
}

# (6) Read protein coordinates from PDB file
coordpdb protein.pdb P

# (7) set calcium chain
segment I {
    auto none
    pdb calcium.pdb
}

# (8) Read water coordinaes from PDB file
coordpdb calcium.pdb I

# (9) write pdb and psf
guesscoord
writepsf rec_r_$aa_old$resid$mutation.psf
writepdb rec_r_$aa_old$resid$mutation.pdb

exit
