---
layout: page
title: About
permalink: /about/
---


This website contains the tutorials and the relative material for the 2nd
module of the Computational Biophysics course, academic year 2019-2020. 


For information on the material presented here, questions, suggestions, please
contact Luca Tubiana, `luca.tubiana _at_ unitn.it`.
